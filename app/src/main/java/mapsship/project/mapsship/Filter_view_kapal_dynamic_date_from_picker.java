package mapsship.project.mapsship;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Filter;

/**
 * Created by plato on 4/1/2016.
 */
public class Filter_view_kapal_dynamic_date_from_picker implements
        Dialog.OnKeyListener,SeekBar.OnSeekBarChangeListener, View.OnClickListener
{
    private Context context;
    private Dialog dialogForm;
    TextView textview_label_time,textview_label_time_Menit;
    DatePicker datePicker;
    SeekBar seekBarHour, seekBarMinute;
    String StringSeekbarJam,StringSeekbarMenit;
    RelativeLayout Layout_Konten_Pcker_From;

    public Filter_view_kapal_dynamic_date_from_picker (Context context)
    {
        this.context = context;
    }

    public void showDialogDatePicker(Activity activity)
    {    Log.e("Tes","showDialogDatePicker");
        int HeightDialog  = (MapsActivity.height/4)*3;


        dialogForm = new Dialog(activity);
            dialogForm.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogForm.setCancelable(false);
            dialogForm.setContentView(R.layout.filter_view_kapal_dynamic_date_from_picker);
            dialogForm.getWindow().setLayout(MapsActivity.width - 20, HeightDialog);

            dialogForm.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            // dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialogForm.setOnKeyListener(this);
            Layout_Konten_Pcker_From = (RelativeLayout) dialogForm.findViewById(R.id.Layout_Konten_Pcker_From);

            datePicker = (DatePicker) dialogForm.findViewById(R.id.datePicker);

            DateFormat dateFormatyear  = new SimpleDateFormat("yyyy");
            DateFormat dateFormatmonth = new SimpleDateFormat("MM");
            DateFormat dateFormatday   = new SimpleDateFormat("dd");

            Date date = new Date();
            int year,month,day;

            year    = Integer.parseInt(String.valueOf(dateFormatyear.format(date)));
            month   = Integer.parseInt(String.valueOf(dateFormatmonth.format(date)))-1;
            day     = Integer.parseInt(String.valueOf(dateFormatday.format(date)));

            // Toast.makeText(context,String.valueOf(dateFormat.format(date)), Toast.LENGTH_LONG).show();

            datePicker.updateDate(year, month, day-1);
            long millis = new java.util.Date().getTime();
             datePicker.setMaxDate(millis);


            Button buttonNow, buttonDone;
            buttonNow = (Button) dialogForm.findViewById(R.id.buttonNow);
            buttonNow.setOnClickListener(this);
            ViewGroup.LayoutParams buttonNowparams = buttonNow.getLayoutParams();
            buttonNowparams.width  = MapsActivity.width/2;
            buttonNow.setLayoutParams(buttonNowparams);

            buttonDone = (Button) dialogForm.findViewById(R.id.buttonDone);
            buttonDone.setOnClickListener(this);
            ViewGroup.LayoutParams buttonDoneparams = buttonDone.getLayoutParams();
            buttonDoneparams.width  = MapsActivity.width/2;
            buttonDone.setLayoutParams(buttonDoneparams);

            textview_label_time       = (TextView) dialogForm.findViewById(R.id.textview_label_time_Jam);
            textview_label_time_Menit = (TextView) dialogForm.findViewById(R.id.textview_label_time_Menit);

            seekBarHour = (SeekBar) dialogForm.findViewById(R.id.seekBarHour);
            seekBarHour.setMax(24);
            seekBarHour.setOnSeekBarChangeListener(this);
            seekBarMinute = (SeekBar) dialogForm.findViewById(R.id.seekBarMinute);
            seekBarMinute.setMax(59);
            seekBarMinute.setOnSeekBarChangeListener(this);
            dialogForm.show();

    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event)
    {    Log.e("Tes","onKey");
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Handler handler = new Handler();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    dialogForm.dismiss();
                }
            });
            Filter_view_kapal_dynamic.CheckAlertPickerForm = true;
            return true;

        }
        return false;
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
    {    Log.e("Tes","onProgressChanged");
        int ProgressLength;
        //Toast.makeText(this.context,"onProgressChanged "+String.valueOf(progress),Toast.LENGTH_SHORT).show();
        switch (seekBar.getId())
        {
            case R.id.seekBarHour:
                ProgressLength =  String.valueOf(progress).length();
                if(ProgressLength==2)
                {
                    StringSeekbarJam = String.valueOf(progress);
                    textview_label_time.setText(StringSeekbarJam);
                }
                else if (ProgressLength==1)
                {
                    StringSeekbarJam = "0"+String.valueOf(progress);
                    textview_label_time.setText(StringSeekbarJam);
                }
                break;
            case R.id.seekBarMinute:
                ProgressLength =  String.valueOf(progress).length();
                if(ProgressLength==2)
                {
                    StringSeekbarMenit = String.valueOf(progress);
                    textview_label_time_Menit.setText(StringSeekbarMenit);
                }
                else if (ProgressLength==1)
                {
                    StringSeekbarMenit = "0"+String.valueOf(progress);
                    textview_label_time_Menit.setText(StringSeekbarMenit);
                }
                break;

        }

       }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar)
    {    Log.e("Tes","onStartTrackingTouch");
        //Toast.makeText(this.context,"onStartTrackingTouch "+seekBar.toString(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar)
    {    Log.e("Tes","onStopTrackingTouch");
        //Toast.makeText(this.context,"onStopTrackingTouch "+seekBar.toString(),Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onClick(View v)
    {
        Log.e("Tes","Onclick");

        switch (v.getId())
        {
            case R.id.buttonDone:
                if (StringSeekbarJam == null)
                {
                    StringSeekbarJam ="00";
                }
                if (StringSeekbarMenit == null)
                {
                    StringSeekbarMenit ="00";
                }

                String bulan = String.valueOf(datePicker.getMonth()+1);
                if (bulan.length() == 1)
                {
                    bulan ="0"+bulan;

                }
               String Hari =  String.valueOf(datePicker.getDayOfMonth());
                if (Hari.length() == 1)
                {
                    Hari = "0"+Hari;
                }

                Filter_view_kapal_dynamic.editTextFrom.setText( Hari + "-" +  bulan
                        + "-" + String.valueOf(datePicker.getYear()) + " " + StringSeekbarJam+":"+StringSeekbarMenit+":"+"00");

                Filter_view_kapal_dynamic.yearForm   = String.valueOf(datePicker.getYear());
                Filter_view_kapal_dynamic.monthForm  = bulan;
                Filter_view_kapal_dynamic.DayForm    = Hari;
                Filter_view_kapal_dynamic.HourForm   = String.valueOf(StringSeekbarJam);
                Filter_view_kapal_dynamic.MinuteForm = String.valueOf(StringSeekbarMenit);
                Filter_view_kapal_dynamic.SecondForm = "00";
                Handler handler = new Handler();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        dialogForm.dismiss();
                    }
                });
                Filter_view_kapal_dynamic.CheckAlertPickerForm = true;


                break;
            case R.id.buttonNow:

              //  DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                DateFormat dateFormatyear  = new SimpleDateFormat("yyyy");
                DateFormat dateFormatmonth = new SimpleDateFormat("MM");
                DateFormat dateFormatday   = new SimpleDateFormat("dd");
                DateFormat dateFormathour  = new SimpleDateFormat("HH");
                DateFormat dateFormatmin   = new SimpleDateFormat("mm");


                Date date = new Date();
                int year,month,day,hour,min;

                year    = Integer.parseInt(String.valueOf(dateFormatyear.format(date)));
                month   = Integer.parseInt(String.valueOf(dateFormatmonth.format(date)))-1;
                day     = Integer.parseInt(String.valueOf(dateFormatday.format(date)));
                hour    = Integer.parseInt(String.valueOf(dateFormathour.format(date)));
                min     = Integer.parseInt(String.valueOf(dateFormatmin.format(date)));

                Filter_view_kapal_dynamic.yearForm   = String.valueOf(year);
                Filter_view_kapal_dynamic.monthForm  = String.valueOf(month);
                Filter_view_kapal_dynamic.DayForm    = String.valueOf(day);
                Filter_view_kapal_dynamic.HourForm   = String.valueOf(hour);
                Filter_view_kapal_dynamic.MinuteForm = String.valueOf(min);

                // Toast.makeText(context,String.valueOf(dateFormat.format(date)), Toast.LENGTH_LONG).show();

                datePicker.updateDate(year, month, day);

              if(String.valueOf(hour).length()==2)
                {
                    seekBarHour.setProgress(hour);
                    textview_label_time.setText(String.valueOf(hour));
                }
              else if (String.valueOf(hour).length()==1)
                {
                    seekBarHour.setProgress(hour);
                    textview_label_time.setText("0" + String.valueOf(hour));
                }
                if(String.valueOf(min).length()==2)
                {
                    seekBarMinute.setProgress(min);
                    textview_label_time_Menit.setText(String.valueOf(min));
                }
                else if (String.valueOf(min).length()==1)
                {
                    seekBarMinute.setProgress(min);
                    textview_label_time_Menit.setText("0" + String.valueOf(min));
                }
                break;
        }

    }

    private void GetData()
    {


    }
}
