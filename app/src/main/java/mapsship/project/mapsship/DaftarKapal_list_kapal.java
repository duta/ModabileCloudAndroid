package mapsship.project.mapsship;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by plato on 3/23/2016.
 */
public class DaftarKapal_list_kapal  extends ArrayAdapter
{

    private final Activity context;
    ArrayList<String> ListNamaKapal = new ArrayList<String>();
    ArrayList<String>  ListMMSI = new ArrayList<String>();
    ArrayList<String>  ListTimeStamp = new ArrayList<String>();

    public DaftarKapal_list_kapal(Activity context, ArrayList<String>  ListNamaKapal ,ArrayList<String> ListMMSI, ArrayList<String> ListTimeStamp)
    {
        super(context, R.layout.daftar_kapal_listview, ListMMSI);
        this.context = context;
        this.ListNamaKapal =  ListNamaKapal;
        this.ListMMSI = ListMMSI;
        this.ListTimeStamp = ListTimeStamp;
    }



    public View getView(int position,View view,ViewGroup parent)
    {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.daftar_kapal_listview, null, true);

        TextView NamaKapal = (TextView) rowView.findViewById(R.id.textview_nama_kapal_listview);
        TextView MMSI = (TextView) rowView.findViewById(R.id.textview_mmsi_listview);
        TextView TimeStamp = (TextView) rowView.findViewById(R.id.textview_time_stamp_lisview);

        NamaKapal.setText(ListNamaKapal.get(position));
        MMSI.setText(ListMMSI.get(position));
        TimeStamp.setText(ListTimeStamp.get(position));


        return rowView;

    }
}
