package mapsship.project.mapsship;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by plato on 3/23/2016.
 */
public class Info_Detail_Kapal  implements Dialog.OnKeyListener,View.OnTouchListener
{

    private Context context;
    private Dialog dialog;
    private RelativeLayout full_screen_information;
    Animation animDown;

    public Info_Detail_Kapal(Context context)
    {
        this.context = context;
    }

    public void showDialog(Activity activity, String ShipNameHashValue, String MMSIHashValue, String CallSignHashValue,
                           String ToBowHashValue, String ToSternHashValue, String ToPortHashValue, String ToStarBoardHashValue,
                           String GrtHashValue, String DestinationHashValue, String ShipTypeDescription, String SOGHashValue,
                           String COGHashValue, String HDGHashValue, String NavStatusDescriptionHashValue, String DraughtHashValue
    ,String StringLatitudeJsonHashValue,String StringLongitudeJsonHashValue,String ETAHourHashValue,String ETAMinuteHashValue,
                           String ETADayHashValue, String ETAMonthHashValue, String TimeStampReceiverHashValue)
    {

        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.full_screen_information);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        full_screen_information = (RelativeLayout) dialog.findViewById(R.id.full_screen_information);
        Animation animUp = AnimationUtils.loadAnimation(this.context, R.anim.anim_up);
        animDown = AnimationUtils.loadAnimation(this.context, R.anim.anim_down);
        Toolbar toolbar_info_fullscreen = (Toolbar) dialog.findViewById(R.id.toolbar_info_fullscreen);
        toolbar_info_fullscreen.setOnTouchListener(this);
        toolbar_info_fullscreen.setTitle(ShipNameHashValue);
        full_screen_information.startAnimation(animUp);

        TextView namaKapalFullScreen, MMSI, Calisign, LOA, Wide, GRT, Destinantion, Type, SOG, COG, Heading, NavStatus, Draught, Posisi, ETA, Timestamp;
        ImageView GambarKapal;

        namaKapalFullScreen = (TextView) dialog.findViewById(R.id.namaKapalFullScreen);
        MMSI = (TextView) dialog.findViewById(R.id.MMSI);
        Calisign = (TextView) dialog.findViewById(R.id.Calisign);
        LOA = (TextView) dialog.findViewById(R.id.LOA);
        Wide = (TextView) dialog.findViewById(R.id.Wide);
        GRT = (TextView) dialog.findViewById(R.id.GRT);
        Destinantion = (TextView) dialog.findViewById(R.id.Destination);
        Type = (TextView) dialog.findViewById(R.id.Type);
        SOG = (TextView) dialog.findViewById(R.id.SOG);
        COG = (TextView) dialog.findViewById(R.id.COG);
        Heading = (TextView) dialog.findViewById(R.id.Heading);
        NavStatus = (TextView) dialog.findViewById(R.id.NavStatus);
        Draught = (TextView) dialog.findViewById(R.id.Draught);
        Posisi = (TextView) dialog.findViewById(R.id.Posisi);
        ETA = (TextView) dialog.findViewById(R.id.ETA);
        Timestamp = (TextView) dialog.findViewById(R.id.Timestamp);
        GambarKapal = (ImageView) dialog.findViewById(R.id.GambarKapal);


        namaKapalFullScreen.setText(ShipNameHashValue);
        MMSI.setText(MMSIHashValue);
        Calisign.setText(CallSignHashValue);
        final Double LOAValue = Double.parseDouble(ToBowHashValue) + Double.parseDouble(ToSternHashValue);
        LOA.setText(String.valueOf(LOAValue) + " m");
        final Double WideValue = Double.parseDouble(ToPortHashValue) + Double.parseDouble(ToStarBoardHashValue);
        Wide.setText(String.valueOf(WideValue) + " m");
        GRT.setText(GrtHashValue);
        Destinantion.setText(DestinationHashValue);
        Type.setText(ShipTypeDescription);
        SOG.setText(SOGHashValue + " knots");
        COG.setText(COGHashValue + " °");
        Heading.setText(HDGHashValue + " °");
        NavStatus.setText(NavStatusDescriptionHashValue);
        Draught.setText(DraughtHashValue);
        DecimalFormat df = new DecimalFormat("#.###");

        Posisi.setText("Lat "+String.valueOf(df.format(Double.parseDouble(StringLatitudeJsonHashValue)))
                +"  "+"Long "+String.valueOf(df.format(Double.parseDouble(StringLongitudeJsonHashValue))));

        String ETAMinuteHashValueEdit, ETAHourHashValueEdit;
        if (ETAHourHashValue.length()==1)
        {
            ETAHourHashValueEdit =  "0"+ETAHourHashValue;
            if (ETAMinuteHashValue.length()==1)
            {

                ETAMinuteHashValueEdit =  "0"+ETAMinuteHashValue;
            }
            else
            {
                ETAMinuteHashValueEdit = ETAMinuteHashValue;

            }

        }
        else
        {
            ETAHourHashValueEdit =  ETAHourHashValue;
            if (ETAMinuteHashValue.length()==1)
            {

                ETAMinuteHashValueEdit =  "0"+ETAMinuteHashValue;
            }
            else
            {
                ETAMinuteHashValueEdit = ETAMinuteHashValue;

            }

        }
        ETA.setText(ETADayHashValue+"-"+ETAMonthHashValue+"  "+ETAHourHashValueEdit+":"+ ETAMinuteHashValueEdit);

        String a = TimeStampReceiverHashValue.replace(".", "");
        String[] parts = a.split("E");
        String input = parts[0];
        DateFormat inputDF = new SimpleDateFormat("yyyyMMddHHmmss");
        DateFormat outputDF = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");


        try
        {
            Date   dateTimeStamp = inputDF.parse(""+input);
            Timestamp.setText(String.valueOf(outputDF.format(dateTimeStamp)));

        } catch (ParseException e) {
            e.printStackTrace();
        }


        dialog.setOnKeyListener(this);
        dialog.show();

        Picasso.with(this.context)
                .load("http://27.111.46.52/modabile.dlu/Uploaded/" + MMSIHashValue + ".png")
                .error(R.drawable.no_image)
                .into(GambarKapal);

    }


    @Override
    public boolean onKey(final DialogInterface dialog, int keyCode, KeyEvent event)
    {

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            full_screen_information.startAnimation(animDown);
            animDown.setAnimationListener(new Animation.AnimationListener()
            {
                @Override
                public void onAnimationStart(Animation animation)
                {
                    // TODO Auto-generated method stub


                }
                @Override
                public void onAnimationRepeat(Animation animation)
                {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onAnimationEnd(Animation animation)
                {

                    Handler handler = new Handler();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    });
                    MapsActivity.AlertDialogInfoKapalDetail = false;

                }
            });
            return true;

        }
        return false;
    }
    float startY = 0;
    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        switch (v.getId())
        {
            case R.id.toolbar_info_fullscreen:

                switch (event.getAction())
                {
                    case MotionEvent.ACTION_DOWN:
                    {
                        startY = event.getY();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    {
                        float endY = event.getY();

                        if (endY > startY)
                        {

                            full_screen_information.startAnimation(animDown);
                            animDown.setAnimationListener(new Animation.AnimationListener()
                            {
                                @Override
                                public void onAnimationStart(Animation animation)
                                {
                                    // TODO Auto-generated method stub

                                }

                                @Override
                                public void onAnimationRepeat(Animation animation)
                                {
                                    // TODO Auto-generated method stub

                                }

                                @Override
                                public void onAnimationEnd(Animation animation)
                                {

                                    Handler handler = new Handler();
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            dialog.dismiss();
                                        }
                                    });
                                    MapsActivity.AlertDialogInfoKapalDetail = false;

                                }
                            });
                        }
                    }

                }
                break;
        }
        return true;
    }
}

