package mapsship.project.mapsship;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by plato on 3/29/2016.
 */
public class Filter_view_kapal implements Dialog.OnKeyListener, View.OnClickListener,CheckBox.OnCheckedChangeListener
{
    private Context context;
    private Dialog dialog;
    CheckBox checkBoxMMSI,checkBoxNamaKapal;
    ArrayAdapter<String> adapterSpinner;
    ArrayAdapter<String> adapterSpinnerKapalKedua;
    ArrayAdapter<String> adapterSpinnerKapalKetiga;
    Spinner spinnerKapalPertama,spinnerKapalKedua,spinnerKapalKetiga;
    ArrayList<String> NamaKapalList = new ArrayList<String>();
    ArrayList<String> MMSIList = new ArrayList<String>();
    ArrayList<String> NamaKapalListKapalKedua = new ArrayList<String>();
    ArrayList<String> MMSIListKapalKedua = new ArrayList<String>();
    ArrayList<String> NamaKapalListKapalKetiga = new ArrayList<String>();
    ArrayList<String> MMSIListKapalKetiga = new ArrayList<String>();
    ArrayList<String> KapalKosong = new ArrayList<String>();
    Boolean CheckMMSI=false,CheckNamaKapal=true;
    int CheckSpinnerKapalPertamaKlik = 0, CheckSpinnerKapalKeduaKlik = 0, CheckSpinnerKapalKetigaKlik = 0;
    String selectedItemTextKapalSatu, selectedItemTextKapalDua,  selectedItemTextKapalTiga;
    String JsonData;
    public Filter_view_kapal (Context context)
    {
        this.context = context;
    }

    public void showDialogFilterKapal(Activity activity)
    {
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.filter_view_kapal);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        Button ButtonProsesFilter = (Button) dialog.findViewById(R.id.ButtonProsesFilter);
        ButtonProsesFilter.setOnClickListener(this);

        spinnerKapalPertama  = (Spinner) dialog.findViewById(R.id.spinnerKapalPertama);
        NamaKapalList.add("Pilih Kapal");
        MMSIList.add("Pilih Kapal");

        spinnerKapalKedua    = (Spinner) dialog.findViewById(R.id.spinnerKapalKedua);
        KapalKosong.add("Data Kosong");

        adapterSpinnerKapalKedua = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner,KapalKosong);
        spinnerKapalKedua.setAdapter(adapterSpinnerKapalKedua);
        spinnerKapalKedua.setEnabled(false);

        spinnerKapalKetiga   = (Spinner) dialog.findViewById(R.id.spinnerKapalKetiga);

        adapterSpinnerKapalKetiga = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner,KapalKosong);
        spinnerKapalKetiga.setAdapter(adapterSpinnerKapalKetiga);
        spinnerKapalKetiga.setEnabled(false);

        checkBoxMMSI = (CheckBox) dialog.findViewById(R.id.checkBoxMMSI);
        checkBoxNamaKapal = (CheckBox) dialog.findViewById(R.id.checkBoxNamaKapal);
        checkBoxMMSI.setOnCheckedChangeListener(this);
        checkBoxNamaKapal.setOnCheckedChangeListener(this);

         spinnerKapalPertama.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
         {

             @Override
             public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id)
             {
                 CheckSpinnerKapalPertamaKlik++;
                 selectedItemTextKapalDua = null;
                 selectedItemTextKapalTiga = null;
                 CheckSpinnerKapalKeduaKlik = 0;
                 if(spinnerKapalKetiga.getCount()!=1 )
                 {
                     if (!NamaKapalListKapalKetiga.isEmpty())
                     {
                         NamaKapalListKapalKetiga.clear();
                         adapterSpinnerKapalKetiga = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner,KapalKosong);
                         spinnerKapalKetiga.setAdapter(adapterSpinnerKapalKetiga);
                         spinnerKapalKetiga.setEnabled(false);
                     }
                     else if (!MMSIListKapalKetiga.isEmpty())
                     {
                         MMSIListKapalKetiga.clear();
                         adapterSpinnerKapalKetiga = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner,KapalKosong);
                         spinnerKapalKetiga.setAdapter(adapterSpinnerKapalKetiga);
                         spinnerKapalKetiga.setEnabled(false);
                     }
                 }


                 if (CheckMMSI == true && CheckSpinnerKapalPertamaKlik > 1)
                    {
                        CheckSpinnerKapalKeduaKlik = 0;
                        MMSIListKapalKedua.clear();
                        NamaKapalListKapalKedua.clear();
                        spinnerKapalKedua.setEnabled(true);

                        selectedItemTextKapalSatu = (String) parentView.getItemAtPosition(position);

                        MMSIListKapalKedua.add("Pilih Kapal");
                        for (int i = 0; i < MMSIList.size(); i++)
                        {
                            if (!selectedItemTextKapalSatu.equalsIgnoreCase(MMSIList.get(i)) && !MMSIList.get(i).equalsIgnoreCase("Pilih Kapal"))
                            {
                                MMSIListKapalKedua.add(MMSIList.get(i));
                            }
                        }
                        adapterSpinnerKapalKedua = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner, MMSIListKapalKedua)
                        {
                            @Override
                            public boolean isEnabled(int position)
                            {
                                if(position == 0)
                                {
                                        // Disable the first item from Spinner
                                        // First item will be use for hint
                                    return false;
                                }
                                else
                                {
                                    return true;
                                }
                            }
                            @Override
                            public View getDropDownView(int position, View convertView,
                                                            ViewGroup parent)
                            {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if(position == 0)
                                {
                                        // Set the hint text color gray
                                    tv.setTextColor(Color.GRAY);
                                }
                                    return view;
                            }
                        };
                        adapterSpinnerKapalKedua.setDropDownViewResource( R.layout.filter_view_kapal_spinner);
                        spinnerKapalKedua.setAdapter(adapterSpinnerKapalKedua);

                    }
                 else if (CheckNamaKapal == true && CheckSpinnerKapalPertamaKlik > 1)
                    {
                        MMSIListKapalKedua.clear();
                        NamaKapalListKapalKedua.clear();
                        spinnerKapalKedua.setEnabled(true);

                        selectedItemTextKapalSatu = (String) parentView.getItemAtPosition(position);

                        NamaKapalListKapalKedua.add("Pilih Kapal");
                        for (int i = 0; i < NamaKapalList.size(); i++)
                            {
                                if (!selectedItemTextKapalSatu.equalsIgnoreCase(NamaKapalList.get(i)) && !MMSIList.get(i).equalsIgnoreCase("Pilih Kapal"))
                                {
                                    NamaKapalListKapalKedua.add(NamaKapalList.get(i));
                                }
                            }
                            adapterSpinnerKapalKedua = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner, NamaKapalListKapalKedua)
                            {
                                @Override
                                public boolean isEnabled(int position)
                                {
                                    if(position == 0)
                                    {
                                        // Disable the first item from Spinner
                                        // First item will be use for hint
                                        return false;
                                    }
                                    else
                                    {
                                        return true;
                                    }
                                }
                                @Override
                                public View getDropDownView(int position, View convertView,
                                                            ViewGroup parent)
                                {
                                    View view = super.getDropDownView(position, convertView, parent);
                                    TextView tv = (TextView) view;
                                    if(position == 0)
                                    {
                                        // Set the hint text color gray
                                        tv.setTextColor(Color.GRAY);
                                    }
                                    return view;
                                }
                            };
                            adapterSpinnerKapalKedua.setDropDownViewResource( R.layout.filter_view_kapal_spinner);
                            spinnerKapalKedua.setAdapter(adapterSpinnerKapalKedua);

                        }


                    }


             @Override
             public void onNothingSelected(AdapterView<?> parentView)
             {

             }
         });

        spinnerKapalKedua.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id)
            {
                CheckSpinnerKapalKeduaKlik++;
                selectedItemTextKapalTiga = null;

                    if (CheckMMSI == true &&  CheckSpinnerKapalKeduaKlik >1)
                    {
                        MMSIListKapalKetiga.clear();
                        NamaKapalListKapalKetiga.clear();
                        spinnerKapalKetiga.setEnabled(true);

                        selectedItemTextKapalDua = (String) parentView.getItemAtPosition(position);

                        MMSIListKapalKetiga.add("Pilih Kapal");
                        for (int i = 0; i < MMSIList.size(); i++)
                        {
                            if (!selectedItemTextKapalDua.equalsIgnoreCase(MMSIList.get(i)) && !selectedItemTextKapalSatu.equalsIgnoreCase(MMSIList.get(i))  && !MMSIList.get(i).equalsIgnoreCase("Pilih Kapal")) {
                                MMSIListKapalKetiga.add(MMSIList.get(i));
                            }
                        }
                        adapterSpinnerKapalKetiga = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner, MMSIListKapalKetiga){
                            @Override
                            public boolean isEnabled(int position){
                                if(position == 0)
                                {

                                    return false;
                                }
                                else
                                {
                                    return true;
                                }
                            }
                            @Override
                            public View getDropDownView(int position, View convertView,
                                                        ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if(position == 0)
                                {
                                    // Set the hint text color gray
                                    tv.setTextColor(Color.GRAY);
                                }
                                return view;
                            }
                        };
                        adapterSpinnerKapalKetiga.setDropDownViewResource( R.layout.filter_view_kapal_spinner);
                        spinnerKapalKetiga.setAdapter(adapterSpinnerKapalKetiga);
                    }
                    else if (CheckNamaKapal == true &&  CheckSpinnerKapalKeduaKlik >1)
                    {
                        MMSIListKapalKetiga.clear();
                        NamaKapalListKapalKetiga.clear();
                        spinnerKapalKetiga.setEnabled(true);

                        selectedItemTextKapalDua = (String) parentView.getItemAtPosition(position);

                        NamaKapalListKapalKetiga.add("Pilih Kapal");
                        for (int i = 0; i < NamaKapalList.size(); i++) {
                            if (!selectedItemTextKapalDua.equalsIgnoreCase(NamaKapalList.get(i)) && !selectedItemTextKapalSatu.equalsIgnoreCase(NamaKapalList.get(i)) && !MMSIList.get(i).equalsIgnoreCase("Pilih Kapal")) {
                                NamaKapalListKapalKetiga.add(NamaKapalList.get(i));
                            }
                        }
                        adapterSpinnerKapalKetiga = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner, NamaKapalListKapalKetiga){
                            @Override
                            public boolean isEnabled(int position){
                                if(position == 0)
                                {
                                    // Disable the first item from Spinner
                                    // First item will be use for hint
                                    return false;
                                }
                                else
                                {
                                    return true;
                                }
                            }
                            @Override
                            public View getDropDownView(int position, View convertView,
                                                        ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if(position == 0)
                                {
                                    // Set the hint text color gray
                                    tv.setTextColor(Color.GRAY);
                                }
                                return view;
                            }
                        };
                        adapterSpinnerKapalKetiga.setDropDownViewResource( R.layout.filter_view_kapal_spinner);
                        spinnerKapalKetiga.setAdapter(adapterSpinnerKapalKetiga);

                    }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView)
            {

            }
        });

        spinnerKapalKetiga.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id)
            {

                CheckSpinnerKapalKetigaKlik++;
                if (CheckMMSI == true &&  CheckSpinnerKapalKeduaKlik >1 || CheckNamaKapal == true &&  CheckSpinnerKapalKeduaKlik > 1)
                {
                    if (position != 0)
                    {
                        selectedItemTextKapalTiga = (String) parentView.getItemAtPosition(position);
                    }

                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }
        });

        String line;
        BufferedReader input = null;
        File file = null;
        StringBuilder textfile = new StringBuilder();
        try
        {

            file = new File(context.getCacheDir(), "MyCacheLastData");
            input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

            StringBuffer buffer = new StringBuffer();
            while ((line = input.readLine()) != null)
            {
                textfile.append(line);
            }

        } catch (IOException e)
        {
            e.printStackTrace();
        }


        try
        {
            JsonData = textfile.toString();
            JSONArray array = new JSONArray(JsonData);
            JSONArray ArrayJsonSemuaKapal = array.getJSONArray(0);
            for (int i = 0; i < ArrayJsonSemuaKapal.length(); i++)
            {
                JSONObject JsonPerKapal =  ArrayJsonSemuaKapal.getJSONObject(i);
                JSONArray features = (JSONArray) JsonPerKapal.get("features");
                for (int j = 0; j <1; j++)
                {
                    JSONObject Feature          = (JSONObject) features.get(j);
                    JSONObject geometry         = (JSONObject) Feature.get("geometry");
                    JSONObject properties       = (JSONObject) Feature.get("properties");
                    String MMSI                 = properties.optString("MMSI");
                    String ShipName             = properties.optString("ShipName");
                    NamaKapalList.add(ShipName);
                    MMSIList.add(MMSI);
                }


            }

        }
        catch (Exception e)
        {

        }


        checkBoxNamaKapal.setChecked(true);
        adapterSpinner=new ArrayAdapter<String>(this.context, R.layout.filter_view_kapal_spinner,NamaKapalList){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0)
                {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                return view;
            }
        };
        adapterSpinner.setDropDownViewResource( R.layout.filter_view_kapal_spinner);
        spinnerKapalPertama.setAdapter(adapterSpinner);

        dialog.show();


    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event)
    {
        dialog.dismiss();
        return true;
    }



    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        switch (buttonView.getId())
        {

            case R.id.checkBoxMMSI:

                selectedItemTextKapalSatu = null;
                selectedItemTextKapalDua = null;
                selectedItemTextKapalTiga = null;
                if (spinnerKapalKedua.getCount()!=1 )
                {
                   // Toast.makeText(context,"spinner kapal dua punya values", Toast.LENGTH_LONG).show();
                     if (!NamaKapalListKapalKedua.isEmpty())
                     {
                         NamaKapalListKapalKedua.clear();
                         adapterSpinnerKapalKedua = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner,KapalKosong);
                         spinnerKapalKedua.setAdapter(adapterSpinnerKapalKedua);
                         spinnerKapalKedua.setEnabled(false);
                     }
                    else if(!MMSIListKapalKedua.isEmpty())
                     {
                         MMSIListKapalKedua.clear();
                         adapterSpinnerKapalKedua = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner,KapalKosong);
                         spinnerKapalKedua.setAdapter(adapterSpinnerKapalKedua);
                         spinnerKapalKedua.setEnabled(false);
                     }
                }
                if(spinnerKapalKetiga.getCount()!=1 )
                {
                    //Toast.makeText(context,"spinner kapal tiga punya values", Toast.LENGTH_LONG).show();
                    if (!NamaKapalListKapalKetiga.isEmpty())
                    {
                        NamaKapalListKapalKetiga.clear();
                        adapterSpinnerKapalKetiga = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner,KapalKosong);
                        spinnerKapalKetiga.setAdapter(adapterSpinnerKapalKetiga);
                        spinnerKapalKetiga.setEnabled(false);
                    }
                    else if (!MMSIListKapalKetiga.isEmpty())
                    {
                        MMSIListKapalKetiga.clear();
                        adapterSpinnerKapalKetiga = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner,KapalKosong);
                        spinnerKapalKetiga.setAdapter(adapterSpinnerKapalKetiga);
                        spinnerKapalKetiga.setEnabled(false);
                    }
                }
                if(checkBoxMMSI.isChecked())
                {   checkBoxMMSI.setEnabled(false);
                    checkBoxNamaKapal.setEnabled(true);
                    checkBoxNamaKapal.setChecked(false);
                }

                CheckMMSI = true;
                CheckNamaKapal = false;
                CheckSpinnerKapalPertamaKlik = 0;
                CheckSpinnerKapalKeduaKlik = 0;
                CheckSpinnerKapalKetigaKlik = 0;

                adapterSpinner=new ArrayAdapter<String>(this.context, R.layout.filter_view_kapal_spinner,MMSIList){
                    @Override
                    public boolean isEnabled(int position){
                        if(position == 0)
                        {
                            // Disable the first item from Spinner
                            // First item will be use for hint
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    @Override
                    public View getDropDownView(int position, View convertView,
                                                ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if(position == 0)
                        {
                            // Set the hint text color gray
                            tv.setTextColor(Color.GRAY);
                        }
                        return view;
                    }
                };
                adapterSpinner.setDropDownViewResource( R.layout.filter_view_kapal_spinner);
                spinnerKapalPertama.setAdapter(adapterSpinner);

                break;
            case  R.id.checkBoxNamaKapal:

                selectedItemTextKapalSatu = null;
                selectedItemTextKapalDua = null;
                selectedItemTextKapalTiga = null;

                if (spinnerKapalKedua.getCount()!=1 )
                {
                    // Toast.makeText(context,"spinner kapal dua punya values", Toast.LENGTH_LONG).show();
                    if (!NamaKapalListKapalKedua.isEmpty())
                    {
                        NamaKapalListKapalKedua.clear();
                        adapterSpinnerKapalKedua = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner,KapalKosong);
                        spinnerKapalKedua.setAdapter(adapterSpinnerKapalKedua);
                        spinnerKapalKedua.setEnabled(false);
                    }
                    else if(MMSIListKapalKedua.isEmpty())
                    {
                        MMSIListKapalKedua.clear();
                        adapterSpinnerKapalKedua = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner,KapalKosong);
                        spinnerKapalKedua.setAdapter(adapterSpinnerKapalKedua);
                        spinnerKapalKedua.setEnabled(false);
                    }
                }
                if(spinnerKapalKetiga.getCount()!=1 )
                {
                    //Toast.makeText(context,"spinner kapal tiga punya values", Toast.LENGTH_LONG).show();
                    if (!NamaKapalListKapalKetiga.isEmpty())
                    {
                        NamaKapalListKapalKetiga.clear();
                        adapterSpinnerKapalKetiga = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner,KapalKosong);
                        spinnerKapalKetiga.setAdapter(adapterSpinnerKapalKetiga);
                        spinnerKapalKetiga.setEnabled(false);
                    }
                    else if (!MMSIListKapalKetiga.isEmpty())
                    {
                        MMSIListKapalKetiga.clear();
                        adapterSpinnerKapalKetiga = new ArrayAdapter<String>(context, R.layout.filter_view_kapal_spinner,KapalKosong);
                        spinnerKapalKetiga.setAdapter(adapterSpinnerKapalKetiga);
                        spinnerKapalKetiga.setEnabled(false);
                    }
                }
                if(checkBoxNamaKapal.isChecked())
                {
                    checkBoxNamaKapal.setEnabled(false);
                    checkBoxMMSI.setEnabled(true);
                    checkBoxMMSI.setChecked(false);
                }

                CheckMMSI = false;
                CheckNamaKapal = true;
                CheckSpinnerKapalPertamaKlik = 0;
                CheckSpinnerKapalKeduaKlik = 0;
                CheckSpinnerKapalKetigaKlik = 0;


                adapterSpinner=new ArrayAdapter<String>(this.context, R.layout.filter_view_kapal_spinner,NamaKapalList){
                    @Override
                    public boolean isEnabled(int position){
                        if(position == 0)
                        {
                            // Disable the first item from Spinner
                            // First item will be use for hint
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    @Override
                    public View getDropDownView(int position, View convertView,
                                                ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if(position == 0)
                        {
                            // Set the hint text color gray
                            tv.setTextColor(Color.GRAY);
                        }
                        return view;
                    }
                };
                adapterSpinner.setDropDownViewResource( R.layout.filter_view_kapal_spinner);
                spinnerKapalPertama.setAdapter(adapterSpinner);

                break;
        }
    }



    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.ButtonProsesFilter:
                MapsActivity.mMap.clear();
                dialog.dismiss();
                Toast.makeText(this.context,  selectedItemTextKapalSatu+"   "+  selectedItemTextKapalDua +""+ selectedItemTextKapalTiga,Toast.LENGTH_LONG).show();
                try
                {
                    JSONArray array = new JSONArray(JsonData);
                    JSONArray ArrayJsonSemuaKapal = array.getJSONArray(0);
                    for (int i = 0; i < ArrayJsonSemuaKapal.length(); i++)
                    {
                        JSONObject JsonPerKapal =  ArrayJsonSemuaKapal.getJSONObject(i);
                        JSONArray features = (JSONArray) JsonPerKapal.get("features");
                        for (int j = 0; j < features.length(); j++)
                        {
                            JSONObject Feature          = (JSONObject) features.get(j);
                            JSONObject geometry         = (JSONObject) Feature.get("geometry");
                            JSONObject properties       = (JSONObject) Feature.get("properties");


                            String MMSI                 = properties.optString("MMSI");
                            String ShipName             = properties.optString("ShipName");
                            if(selectedItemTextKapalSatu.equalsIgnoreCase(MMSI) || selectedItemTextKapalSatu.equalsIgnoreCase(ShipName))
                            {
                                JSONArray  coordinates      = (JSONArray) geometry.get("coordinates");
                                String StringLatitudeJson = String.valueOf(coordinates.get(1));
                                String StringLongitudeJson = String.valueOf(coordinates.get(0));;
                                Double LatitudeJson = Double.valueOf(StringLatitudeJson);
                                Double LongitudeJson = Double.valueOf(StringLongitudeJson);
                                LatLng Kordinat    = new LatLng(LatitudeJson,LongitudeJson);
                                MapsActivity.markerName =   MapsActivity.mMap.addMarker(new MarkerOptions().position(Kordinat).icon(BitmapDescriptorFactory.fromResource(R.drawable.jalur_kapal)).title(ShipName));

                            }




                           /* JSONObject Feature          = (JSONObject) features.get(j);
                            JSONObject geometry         = (JSONObject) Feature.get("geometry");
                            JSONObject properties       = (JSONObject) Feature.get("properties");
                            String TimeStampReceiver    = properties.optString("TimeStampReceiver");
                            String ToStern              = properties.optString("ToStern");
                            String ROT                  = properties.optString("ROT");
                            String ShipType             = properties.optString("ShipType");
                            String IMONumber            = properties.optString("IMONumber");
                            String DangerHead           = properties.optString("DangerHead");
                            String Draught              = properties.optString("Draught");
                            String ETAMinute            = properties.optString("ETAMinute");
                            String ETADay               = properties.optString("ETADay");
                            String NavStatus            = properties.optString("NavStatus");
                            String StationID            = properties.optString("StationID");
                            String CallSign             = properties.optString("CallSign");
                            String ZoneID               = properties.optString("ZoneID");
                            String COG                  = properties.optString("COG");
                            String HDG                  = properties.optString("HDG");
                            String ETAHour              = properties.optString("ETAHour");
                            String StationName          = properties.optString("StationName");
                            String ZoneName             = properties.optString("ZoneName");
                            String Grt                  = properties.optString("Grt");
                            String ToPort               = properties.optString("ToPort");
                            String ETAMonth             = properties.optString("ETAMonth");
                            String SOG                  = properties.optString("SOG");
                            String DangerDist           = properties.optString("DangerDist");
                            String ToBow                = properties.optString("ToBow");
                            String GrtBySiuk            = properties.optString("GrtBySiuk");
                            String ToStarBoard          = properties.optString("ToStarBoard");
                            String NavStatusDescription = properties.optString("NavStatusDescription");
                            String Destination          = properties.optString("Destination");
                            String MMSI                 = properties.optString("MMSI");
                            String ShipTypeDescription  = properties.optString("ShipTypeDescription");
                            String ShipName             = properties.optString("ShipName");
                            String type                 = geometry.optString("type");
                            JSONArray  coordinates      = (JSONArray) geometry.get("coordinates");
                            String StringLatitudeJson  = String.valueOf(coordinates.get(1));
                            String StringLongitudeJson = String.valueOf(coordinates.get(0));;

                            Double LatitudeJson = Double.valueOf(StringLatitudeJson);
                            Double LongitudeJson = Double.valueOf(StringLongitudeJson);
                            LatLng Kordinat    = new LatLng(LatitudeJson,LongitudeJson);*/
                        }


                    }

                }
                catch (Exception e)
                {

                }

                break;
        }
    }
}
