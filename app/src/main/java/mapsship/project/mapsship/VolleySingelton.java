package mapsship.project.mapsship;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by ILHAM on 4/15/2016.
 */
public class VolleySingelton
{

    private static  VolleySingelton instance;
    private RequestQueue requestQueue;

    private VolleySingelton (Context context)
    {
        requestQueue = Volley.newRequestQueue(context);
    }

    public static  VolleySingelton getInstance(Context context)
    {
        if (instance == null)
        {
            instance = new  VolleySingelton (context);
        }
        return instance;
    }

    public RequestQueue getRequestQueue()
    {
        return requestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req)
    {
        req.setTag("App");

        getRequestQueue().add(req);

    }
}
