package mapsship.project.mapsship;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by plato on 3/21/2016.
 */
public class CustomListAdapterListSearchResult extends ArrayAdapter
{
    private final Activity context;
    ArrayList<String> SearchKey = new ArrayList<String>();
    ArrayList<String>  SearchKeyName = new ArrayList<String>();
    public CustomListAdapterListSearchResult(Activity context, ArrayList<String>  SearchKey ,ArrayList<String> SearchKeyName)
    {
        super(context, R.layout.listview_result_search, SearchKey);
        this.context = context;
        this.SearchKey = SearchKey;
        this.SearchKeyName = SearchKeyName;
    }

    public View getView(int position,View view,ViewGroup parent)
    {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.listview_result_search, null, true);

        TextView SearchKeyTextview = (TextView) rowView.findViewById(R.id.SearchKey);
        TextView SearchKeyNameTextview = (TextView) rowView.findViewById(R.id.SearchKeyName);


        SearchKeyTextview.setText(SearchKey.get(position));
        SearchKeyNameTextview.setText(SearchKeyName.get(position));

        return rowView;

    }
}
