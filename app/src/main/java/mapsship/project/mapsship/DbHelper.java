package mapsship.project.mapsship;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by plato on 3/21/2016.
 */
public class DbHelper extends SQLiteOpenHelper
{
    private static final String db_name                    = "DLU-VSL.db";
    private static final int db_version=1;
    public static final String TABLE_NAME                  = "Json_Data_Kapal";
   /* public static final String COLUMN_TimeStampReceiver    = "TimeStampReceiver";
    public static final String COLUMN_ToStern              = "ToStern";
    public static final String COLUMN_ROT                  = "ROT";
    public static final String COLUMN_ShipType             = "ShipType";
    public static final String COLUMN_IMONumber            = "IMONumber";
    public static final String COLUMN_DangerHead           = "DangerHead";
    public static final String COLUMN_Draught              = "Draught";
    public static final String COLUMN_ETAMinute            = "ETAMinute";
    public static final String COLUMN_ETADay               = "ETADay";
    public static final String COLUMN_NavStatus            = "NavStatus";
    public static final String COLUMN_StationID            = "StationID";
    public static final String COLUMN_CallSign             = "CallSign";
    public static final String COLUMN_ZoneID               = "ZoneID";
    public static final String COLUMN_COG                  = "COG";
    public static final String COLUMN_HDG                  = "HDG";
    public static final String COLUMN_ETAHour              = "ETAHour";
    public static final String COLUMN_StationName          = "StationName";
    public static final String COLUMN_ZoneName             = "ZoneName";
    public static final String COLUMN_Grt                  = "Grt";
    public static final String COLUMN_ToPort               = "ToPort";
    public static final String COLUMN_ETAMonth             = "ETAMonth";
    public static final String COLUMN_SOG                  = "SOG";
    public static final String COLUMN_DangerDist           = "DangerDist";
    public static final String COLUMN_ToBow                = "ToBow";
    public static final String COLUMN_GrtBySiuk            = "GrtBySiuk";
    public static final String COLUMN_ToStarBoard          = "ToStarBoard";
    public static final String COLUMN_NavStatusDescription = "NavStatusDescription";
    public static final String COLUMN_Destination          = "Destination";
    public static final String COLUMN_MMSI                 = "MMSI";
    public static final String COLUMN_ShipTypeDescription  = "ShipTypeDescription";
    public static final String COLUMN_ShipName             = "ShipName";
    public static final String COLUMN_type                 = "type";
    public static final String COLUMN_StringLatitudeJson   = "StringLatitudeJson";
    public static final String COLUMN_StringLongitudeJson  = "StringLongitudeJson";*/



    public static final String COLUMN_MMSI                 = "MMSI";
    public static final String COLUMN_ShipName             = "ShipName";
    public static final String COLUMN_StringLatitudeJson   = "StringLatitudeJson";
    public static final String COLUMN_StringLongitudeJson  = "StringLongitudeJson";


    private static final String db_create = "CREATE TABLE " + TABLE_NAME + "("
            + COLUMN_MMSI + " TEXT PRIMARY KEY," + COLUMN_ShipName +" TEXT," + COLUMN_StringLatitudeJson +" TEXT,"
            + COLUMN_StringLongitudeJson +" TEXT" + ")";

    // Perintah SQL untuk membuat tabel database baru
   /* private static final String db_create = "CREATE TABLE " + TABLE_NAME + "("
            + COLUMN_TimeStampReceiver +" TEXT," + COLUMN_ToStern +" TEXT,"
            + COLUMN_ROT +" TEXT," + COLUMN_ToStern +" TEXT," + COLUMN_ToStern +" TEXT,"
            + COLUMN_ToStern +" TEXT," + COLUMN_ShipType +" TEXT," + COLUMN_IMONumber +" TEXT,"
            + COLUMN_DangerHead +" TEXT," + COLUMN_Draught +" TEXT," + COLUMN_ETAMinute +" TEXT,"
            + COLUMN_ETADay +" TEXT," + COLUMN_NavStatus +" TEXT," + COLUMN_StationID +" TEXT,"
            + COLUMN_CallSign +" TEXT," + COLUMN_ZoneID +" TEXT," + COLUMN_COG +" TEXT,"
            + COLUMN_HDG +" TEXT," + COLUMN_ETAHour +" TEXT," + COLUMN_StationName +" TEXT,"
            + COLUMN_ZoneName +" TEXT," + COLUMN_Grt +" TEXT," + COLUMN_ToPort +" TEXT,"
            + COLUMN_ETAMonth +" TEXT," + COLUMN_SOG +" TEXT," + COLUMN_DangerDist +" TEXT,"
            + COLUMN_ToBow +" TEXT," + COLUMN_GrtBySiuk +" TEXT," + COLUMN_ToStarBoard +" TEXT,"
            + COLUMN_NavStatusDescription +" TEXT,"  + COLUMN_Destination + " TEXT,"
            + COLUMN_MMSI + " TEXT," + COLUMN_ShipTypeDescription +" TEXT," + COLUMN_ShipName +" TEXT,"
            + COLUMN_type + " TEXT," + COLUMN_StringLatitudeJson +" TEXT," + COLUMN_StringLongitudeJson +" TEXT"+")";*/

    public DbHelper(Context context)
    {
        super(context, db_name, null, db_version);
        // Auto generated
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(db_create);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
