package mapsship.project.mapsship;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by plato on 3/22/2016.
 */
public class DaftarKapal implements Dialog.OnKeyListener,ListView.OnItemClickListener,View.OnTouchListener
{

    private Context context;
    private Dialog dialog;
    private RelativeLayout full_screen_information;
    private Animation animDown;
    ArrayList<String> ListNamaKapal = new ArrayList<String>();
    ArrayList<String>  ListMMSI = new ArrayList<String>();
    ArrayList<String>  ListTimeStamp = new ArrayList<String>();
    ArrayList<String>  LatKapal = new ArrayList<String>();
    ArrayList<String>  LongtKapal = new ArrayList<String>();

    Navigation_page alertNavigation;
    public DaftarKapal (Context context)
    {
        this.context=context;
    }

    public void showDialog(Activity activity, ArrayList<String> ListNamaKapal,ArrayList<String>  ListMMSI,
                           ArrayList<String>  ListTimeStamp,ArrayList<String>  LatKapal,ArrayList<String>   LongtKapal)
    {

        this.ListNamaKapal = ListNamaKapal;
        this.ListMMSI= ListMMSI;
        this.ListTimeStamp = ListTimeStamp;
        this.LatKapal = LatKapal;
        this.LongtKapal =  LongtKapal;

      //  alertNavigation = new Navigation_page(this.context, ListNamaKapalNav, ListMMSINav, ListTimeStampNav, LatKapalNav,  LongtKapalNav);
        alertNavigation = new Navigation_page(this.context, ListNamaKapal, ListMMSI, ListTimeStamp, LatKapal,  LongtKapal);

        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.daftar_kapal);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        full_screen_information = (RelativeLayout) dialog.findViewById(R.id.full_screen_information);
        Animation animUp = AnimationUtils.loadAnimation(this.context, R.anim.anim_up);

        animDown = AnimationUtils.loadAnimation(this.context, R.anim.anim_down);
        full_screen_information.startAnimation(animUp);


        Toolbar toolbar = (Toolbar) dialog.findViewById(R.id.toolbar_list_kapal);
        toolbar.setOnTouchListener(this);
        toolbar.setTitle("Jumlah Kapal Terdeteksi " + String.valueOf(ListNamaKapal.size()));
        ArrayList<String> TimeStampAfterFormat =  new ArrayList<String>();


        for (int i = 0 ;i<ListTimeStamp.size(); i++)
        {
            String a = ListTimeStamp.get(i).replace(".", "");
            String[] parts = a.split("E");
            String input = parts[0];
            DateFormat inputDF = new SimpleDateFormat("yyyyMMddHHmmss");
            DateFormat outputDF = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

            try
            {
                Date  Waktu  = inputDF.parse(""+input);
                TimeStampAfterFormat.add(String.valueOf(outputDF.format(Waktu)));

            }
            catch (ParseException e)
            {
                if(input.length()!=14)
                {
                    try
                    {
                        Date Waktu  = inputDF.parse(""+input+"00");
                        TimeStampAfterFormat.add(String.valueOf(outputDF.format(Waktu)));
                    }
                    catch (ParseException error)
                    {
                        TimeStampAfterFormat.add(error.toString());
                    }
                }
            }

        }
        ListView listkapaltampil = (ListView) dialog.findViewById(R.id.listview_daftar_kapal);
        listkapaltampil.setOnItemClickListener(this);
        //DaftarKapal_list_kapal adapter = new DaftarKapal_list_kapal(activity, ListNamaKapal,ListMMSI,ListTimeStamp);
       DaftarKapal_list_kapal adapter = new DaftarKapal_list_kapal(activity, ListNamaKapal,ListMMSI,TimeStampAfterFormat);

        listkapaltampil.setAdapter(adapter);
        dialog.setOnKeyListener(this);
        dialog.show();

    }


   @Override
    public boolean onKey(final DialogInterface dialog, int keyCode, KeyEvent event)
    {

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            full_screen_information.startAnimation(animDown);
            animDown.setAnimationListener(new Animation.AnimationListener()
            {
                @Override
                public void onAnimationStart(Animation animation) {
                    // TODO Auto-generated method stub


                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    Handler handler = new Handler();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    });
                    alertNavigation.showDialog((Activity) context);
                }
            });
            return true;

        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {

       // String Slecteditem = ListMMSI.get(position);
        /*Navigation_page  alertNavigation = new Navigation_page(this.context, ListNamaKapal, ListMMSI, ListTimeStamp, LatKapal,  LongtKapal);
        alertNavigation.DialogDissmiss();*/
        MapsActivity.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(LatKapal.get(position)), Double.parseDouble(LongtKapal.get(position))), 17));
        full_screen_information.startAnimation(animDown);
        animDown.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {
                // TODO Auto-generated method stub


            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                Handler handler = new Handler();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                });
            }
        });
        //Toast.makeText(this.context,Slecteditem,Toast.LENGTH_LONG).show();
    }
    float startY = 0;
    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        switch (v.getId())
        {
            case R.id.toolbar_list_kapal:

                switch (event.getAction())
                {
                    case MotionEvent.ACTION_DOWN:
                    {
                        startY = event.getY();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    {
                        float endY = event.getY();

                        if (endY > startY)
                        {

                            full_screen_information.startAnimation(animDown);
                            animDown.setAnimationListener(new Animation.AnimationListener()
                            {
                                @Override
                                public void onAnimationStart(Animation animation)
                                {
                                    // TODO Auto-generated method stub


                                }

                                @Override
                                public void onAnimationRepeat(Animation animation)
                                {
                                    // TODO Auto-generated method stub

                                }

                                @Override
                                public void onAnimationEnd(Animation animation)
                                {

                                    dialog.dismiss();
                                    alertNavigation.showDialog((Activity) context);
                                }
                            });
                        }

                    }

                }
                break;

        }
        return true;
    }
}
