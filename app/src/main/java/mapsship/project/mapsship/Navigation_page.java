package mapsship.project.mapsship;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by plato on 3/29/2016.
 */
public class Navigation_page implements  Dialog.OnKeyListener,View.OnTouchListener, ListView.OnItemClickListener
{

    private Context context;
    private Dialog dialog;
    RelativeLayout Navigation_page;
    Animation right_to_left;
    ArrayList<String>  ListNamaKapal = new ArrayList<String>();
    ArrayList<String>  ListMMSI = new ArrayList<String>();
    ArrayList<String>  ListTimeStamp = new ArrayList<String>();
    ArrayList<String>  LatKapal = new ArrayList<String>();
    ArrayList<String>  LongtKapal = new ArrayList<String>();

    public  Navigation_page(Context context, ArrayList<String>  ListNamaKapal,  ArrayList<String>  ListMMSI,
                            ArrayList<String>  ListTimeStamp,  ArrayList<String>  LatKapal, ArrayList<String>  LongtKapal)
    {
        this.context = context;
        this.ListNamaKapal =  ListNamaKapal;
        this.ListMMSI = ListMMSI;
        this.ListTimeStamp = ListTimeStamp;
        this.LatKapal = LatKapal;
        this.LongtKapal = LongtKapal;
    }


    public void showDialog(Activity activity)
    {
        int widthDialog = MapsActivity.width/4;

        Log.e("THREAD JUMLAH", String.valueOf(java.lang.Thread.activeCount()));
        Log.e("Thread", Thread.currentThread().getName());
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.navigation_page);
        dialog.getWindow().setLayout(widthDialog*3, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setGravity(Gravity.LEFT);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        Animation left_to_right = AnimationUtils.loadAnimation(this.context, R.anim.left_to_right);
        right_to_left  = AnimationUtils.loadAnimation(this.context, R.anim.right_to_left);
        Navigation_page = (RelativeLayout) dialog.findViewById(R.id.Navigation_Page);
        Navigation_page.setOnTouchListener(this);
        Navigation_page.startAnimation(left_to_right);


        ArrayList<Integer> GambarMenu = new ArrayList<Integer>();
        ArrayList<String> NamaMenu = new ArrayList<String>();

        GambarMenu.add(R.drawable.cancelsearch);
        GambarMenu.add(R.drawable.cancelsearch);
        GambarMenu.add(R.drawable.cancelsearch);

        NamaMenu.add("Daftar Kapal");
        NamaMenu.add("Recorded");
        NamaMenu.add("Tampilkan Seluruh Kapal");

        ListView listViewNavigationPage =(ListView) dialog.findViewById(R.id.listViewNavigationPage);
        listViewNavigationPage.setOnItemClickListener(this);
        Navigation_Page_Listview adapter = new Navigation_Page_Listview(activity, GambarMenu,NamaMenu);
        listViewNavigationPage.setAdapter(adapter);


        dialog.setOnKeyListener(this);
        dialog.show();

    }



    @Override
    public boolean onKey(final DialogInterface dialog, int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
           Navigation_page.startAnimation(right_to_left);
            right_to_left.setAnimationListener(new Animation.AnimationListener()
            {
                @Override
                public void onAnimationStart(Animation animation)
                {
                    // TODO Auto-generated method stub


                }

                @Override
                public void onAnimationRepeat(Animation animation)
                {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onAnimationEnd(Animation animation)
                {

                    Handler handler = new Handler();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    });

                    MapsActivity.AlertDialogDaftarKapal = false;

                }
            });
            return true;
        }
        return false;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {   switch (v.getId())
        {
            case R.id.Navigation_Page:
                break;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        if(position == 0)
        {
            Navigation_page.startAnimation(right_to_left);
            right_to_left.setAnimationListener(new Animation.AnimationListener()
            {
                @Override
                public void onAnimationStart(Animation animation)
                {
                    // TODO Auto-generated method stub


                }

                @Override
                public void onAnimationRepeat(Animation animation)
                {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onAnimationEnd(Animation animation)
                {
                    Handler handler = new Handler();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    });

                    DaftarKapal alert = new DaftarKapal(context);
                    alert.showDialog((Activity) context, ListNamaKapal, ListMMSI, ListTimeStamp, LatKapal, LongtKapal);
                    MapsActivity.AlertDialogDaftarKapal = false;
                }
            });


        }
        if(position == 1)
        {

            Navigation_page.startAnimation(right_to_left);
            right_to_left.setAnimationListener(new Animation.AnimationListener()
            {
                @Override
                public void onAnimationStart(Animation animation)
                {
                    // TODO Auto-generated method stub


                }
                @Override
                public void onAnimationRepeat(Animation animation)
                {
                    // TODO Auto-generated method stub

                }
                @Override
                public void onAnimationEnd(Animation animation)
                {

                    Handler handler = new Handler();
                    handler.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            dialog.dismiss();
                        }
                    });
                   /* Filter_view_kapal alert = new Filter_view_kapal(context);
                    alert.showDialogFilterKapal((Activity) context);*/
                    Filter_view_kapal_dynamic alert = new Filter_view_kapal_dynamic(context);
                    alert.showDialogFilter_view_kapal_dynamic((Activity)context);
                }
            });

        }
        if (position == 2)
        {

        }

    }
}
