package mapsship.project.mapsship;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.telephony.SignalStrength;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by plato on 3/31/2016.
 */
public class Filter_view_kapal_dynamic implements View.OnClickListener,View.OnTouchListener,
        ListView.OnItemClickListener,Dialog.OnKeyListener,Spinner.OnItemSelectedListener,SeekBar.OnSeekBarChangeListener
{

    private Context context;
    private Dialog dialog;
    private RelativeLayout LayoutParentListViewMMSIsearch;
    ListView ListviewKapalMMSI;
    ArrayList<String> MMSISubmit = new ArrayList<String>();
    ArrayList<String>  NamaKapalSubmit = new ArrayList<String>();
    Filter_view_kapal_dynamic_listview_adaptor adapter;
    EditText editTextMMSI;
    public  static EditText  editTextFrom, editTextTo;
    String SelectedMMSI, SelectedNamaKapal ;
    TextView KapalSelectSatu,KapalSelectDua,KapalSelectTiga,KapalSelectEmpat,KapalSelectLima;
    RelativeLayout Jarak1;
    String Index1,Index2;
    Cursor cursor;
    static ArrayList<String> MMSItersubmit = new ArrayList<String>();
    Navigation_page alertNavigation;
    Boolean ListviewItem = false;
    Boolean CheckBukanDataKembar = true;
    public static String yearForm,monthForm,DayForm,HourForm,MinuteForm,SecondForm;
    public static String yearTo,monthTo,DayTo,HourTo,MinuteTo,SecondTo;
    RelativeLayout layout_filter_view_kapal_dynamic;
    RelativeLayout LayoutStatusKapalTerpilih;
    TextView StatusKapalTerpilih;

    ArrayList<String> ListNamaKapal = new ArrayList<String>();
    ArrayList<String>  ListMMSI = new ArrayList<String>();
    ArrayList<String>  ListTimeStamp = new ArrayList<String>();
    ArrayList<String>  LatKapal = new ArrayList<String>();
    ArrayList<String>  LongtKapal = new ArrayList<String>();
    int widthEditText;
    Animation  animDown, animUp;
    Button ButtonTracking;
    public static   Boolean CheckDialog = true;
    RelativeLayout LayoutKapalSelectSatu,LayoutKapalSelectDua,LayoutKapalSelectTiga,
            LayoutKapalSelectEmpat,LayoutKapalSelectLima,LayoutTextviewAddKapal;
    Button buttonDeleteKapalSatu,buttonDeleteKapalDua,buttonDeleteKapalTiga,buttonDeleteKapalEmpat,buttonDeleteKapalLima;

    ArrayList<String>  KapaldalamListSubmit = new ArrayList<String>();
    ArrayList<String>  MMSIdalamListSubmit = new ArrayList<String>();
    static Boolean CheckAlertPickerForm = true,CheckAlertPickerTo = true;
    SeekBar seekBarInterval;
    Spinner SpinnerJenisInterval;
    Boolean CheckLayoutParentListViewMMSIsearch = false;
    TextView TextviewIntervalValueSeekbar;
    String SelectedInterval;
    TextView textView;
    Boolean TitikAwalKapal = true, CameraAwal = false;

    List<LatLng> valuesBatasAwalDanAkhir = new ArrayList<LatLng>();
    ArrayList<LatLng> points = new ArrayList<LatLng>();
    Polyline line;
    ProgressBar circleProgres;
    int progressIntervalMinute,progressIntervalHour;
    static ArrayList <Date> daftarWaktu = new ArrayList<>();
    String MMSIPerbandingan = null;
    static String mmsi_params;
    String JsonData;
    boolean ValidityDate = false;
    ArrayList<JSONArray> ArrayListJson = new ArrayList<>();
    JSONObject jsonResult;
    JSONArray JsonArray;
    boolean awalKapal=true;

    int finalI;
    int finalX;
    Boolean CheckVolley = false;
    int responeJumlah;
    int  MMSItersubmitParam;
    Button ButtonAdd;
    Dialog mOverlayDialog;
    public Filter_view_kapal_dynamic (Context context)
    {
        this.context = context;
    }

    public void showDialogFilter_view_kapal_dynamic(Activity activity)
    {
        MMSItersubmit.clear();
        NamaKapalSubmit.clear();

        widthEditText  = MapsActivity.width/4;
        alertNavigation = new Navigation_page(this.context, ListNamaKapal, ListMMSI, ListTimeStamp, LatKapal,  LongtKapal);

        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.filter_view_kapal_dynamic);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setOnKeyListener(this);

        layout_filter_view_kapal_dynamic = (RelativeLayout) dialog.findViewById(R.id.layout_filter_view_kapal_dynamic);
        animUp = AnimationUtils.loadAnimation(this.context, R.anim.anim_up);

        circleProgres = (ProgressBar) dialog.findViewById(R.id.circleProgres);
        animDown = AnimationUtils.loadAnimation(this.context, R.anim.anim_down);
        layout_filter_view_kapal_dynamic.startAnimation(animUp);


        ButtonTracking = (Button) dialog.findViewById(R.id.ButtonTracking);
        ButtonTracking.setOnClickListener(this);

        ButtonAdd = (Button) dialog.findViewById(R.id.buttonAdd);
        ButtonAdd.setOnClickListener(this);

        LayoutTextviewAddKapal = (RelativeLayout) dialog.findViewById(R.id.LayoutTextviewAddKapal);

        LayoutParentListViewMMSIsearch = (RelativeLayout) dialog.findViewById(R.id.LayoutParentListViewMMSISearch);
        adapter = new Filter_view_kapal_dynamic_listview_adaptor((Activity) context, MMSISubmit, NamaKapalSubmit);
        ListviewKapalMMSI = (ListView) dialog.findViewById(R.id.ListviewKapalMMSI);
        ListviewKapalMMSI.setOnItemClickListener(this);

        ListviewKapalMMSI.setOnTouchListener(new View.OnTouchListener()
        {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        KapalSelectSatu  =(TextView) dialog.findViewById(R.id.KapalSelectSatu);
        KapalSelectDua   =(TextView) dialog.findViewById(R.id.KapalSelectDua);
        KapalSelectTiga  =(TextView) dialog.findViewById(R.id.KapalSelectTiga);
        KapalSelectEmpat =(TextView) dialog.findViewById(R.id.KapalSelectEmpat);
        KapalSelectLima  =(TextView) dialog.findViewById(R.id.KapalSelectLima);


        LayoutStatusKapalTerpilih = (RelativeLayout) dialog.findViewById(R.id.LayoutStatusKapalTerpilih);
        StatusKapalTerpilih = (TextView) dialog.findViewById(R.id.StatusKapalTerpilih);
        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        p.addRule(RelativeLayout.CENTER_VERTICAL, R.id.LayoutStatusKapalTerpilih);
        p.setMargins(0, 0, 0, 10);
        StatusKapalTerpilih.setLayoutParams(p);

        LayoutKapalSelectSatu = (RelativeLayout) dialog.findViewById(R.id.LayoutKapalSelectSatu);
        LayoutKapalSelectDua = (RelativeLayout) dialog.findViewById(R.id.LayoutKapalSelectDua);
        LayoutKapalSelectTiga = (RelativeLayout) dialog.findViewById(R.id.LayoutKapalSelectTiga);
        LayoutKapalSelectEmpat = (RelativeLayout) dialog.findViewById(R.id.LayoutKapalSelectEmpat);
        LayoutKapalSelectLima = (RelativeLayout) dialog.findViewById(R.id.LayoutKapalSelectLima);

        buttonDeleteKapalSatu = (Button) dialog.findViewById(R.id.buttonDeleteKapalSatu);
        buttonDeleteKapalSatu.setOnClickListener(this);
        buttonDeleteKapalDua = (Button) dialog.findViewById(R.id.buttonDeleteKapalDua);
        buttonDeleteKapalDua.setOnClickListener(this);
        buttonDeleteKapalTiga = (Button) dialog.findViewById(R.id.buttonDeleteKapalTiga);
        buttonDeleteKapalTiga.setOnClickListener(this);
        buttonDeleteKapalEmpat = (Button) dialog.findViewById(R.id.buttonDeleteKapalEmpat);
        buttonDeleteKapalEmpat.setOnClickListener(this);
        buttonDeleteKapalLima = (Button) dialog.findViewById(R.id.buttonDeleteKapalLima);
        buttonDeleteKapalLima.setOnClickListener(this);
        editTextFrom();

        editTextTo();

        Jarak1 = (RelativeLayout) dialog.findViewById(R.id.Jarak1);
        MMSIeditText();

        seekBarInterval = (SeekBar) dialog.findViewById(R.id.seekBarInterval);
        seekBarInterval.setOnSeekBarChangeListener(this);
       /* ViewGroup.LayoutParams   seekBarIntervalparams =    seekBarInterval.getLayoutParams();
        seekBarIntervalparams.width  =   widthEditText*3;
        seekBarInterval.setLayoutParams(seekBarIntervalparams);*/

        ArrayList<String> arraySpinner = new ArrayList<String>();
        arraySpinner.add("Minute");
        arraySpinner.add("Hour");



        ArrayAdapter<String>  adapter  = new ArrayAdapter<String>(
                this.context,R.layout.filter_view_kapal_dynamic_spinner_textview,arraySpinner);

        SpinnerJenisInterval = (Spinner) dialog.findViewById(R.id.spinnerJenisInterval);
        SpinnerJenisInterval.setOnItemSelectedListener(this);
        //   ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.context, android.R.layout.simple_spinner_item, arraySpinner);
        SpinnerJenisInterval.setAdapter(adapter);
        String myString = "Hour"; //the value you want the position for
        ArrayAdapter myAdap = (ArrayAdapter) SpinnerJenisInterval.getAdapter(); //cast to an ArrayAdapter
        int spinnerPosition = myAdap.getPosition(myString);
        //set the default according to value
        SpinnerJenisInterval.setSelection(spinnerPosition);


        TextviewIntervalValueSeekbar = (TextView)dialog.findViewById(R.id.TextviewIntervalValueSeekbar);
        /*CheckBox ShowLabel, ShowTrail,ShowLoa;
        ShowLabel = (CheckBox) dialog.findViewById(R.id.ShowLabel);
        ShowLoa  = (CheckBox) dialog.findViewById(R.id.ShowLoa);
        ShowTrail = (CheckBox) dialog.findViewById(R.id.ShoWTrail);

        ViewGroup.LayoutParams  ShowLabelparams =    ShowLabel.getLayoutParams();
        ShowLabelparams.width = widthEditText*2;
        ShowLabel.setLayoutParams(ShowLabelparams);

        ViewGroup.LayoutParams  ShowLoaparams =     ShowLoa.getLayoutParams();
        ShowLoaparams.width = widthEditText*2;
        ShowLoa.setLayoutParams(ShowLoaparams);*/

        dialog.show();

    }
    public void editTextFrom()
    {
        editTextFrom = (EditText) dialog.findViewById(R.id.editTextFrom);

        DateFormat dateFormatyear  = new SimpleDateFormat("yyyy");
        DateFormat dateFormatmonth = new SimpleDateFormat("MM");
        DateFormat dateFormatday   = new SimpleDateFormat("dd");
        DateFormat dateFormathour  = new SimpleDateFormat("HH");
        DateFormat dateFormatmin   = new SimpleDateFormat("mm");

        Date date = new Date();
       // int year,month,day,hour,min;

        yearForm        = String.valueOf(dateFormatyear.format(date));
        if(yearForm.length() == 1)
        {
            yearForm = "0"+yearForm;
        }
        monthForm       = String.valueOf(dateFormatmonth.format(date));
        if (monthForm.length() == 1)
        {
            monthForm = "0"+monthForm;
        }

        DayForm         = String.valueOf(Integer.parseInt(dateFormatday.format(date))-1);
       // DayForm         = String.valueOf(dateFormatday.format(date));
        if(DayForm.length() == 1)
        {
            DayForm = "0"+DayForm;
        }
        HourForm        = String.valueOf(dateFormathour.format(date));
        if(HourForm.length() == 1)
        {
            HourForm   = "0"+DayForm;
        }
        MinuteForm      = String.valueOf(dateFormatmin.format(date));
        if(MinuteForm.length() == 1)
        {
            MinuteForm = "0"+MinuteForm;
        }
        SecondForm      = "00";

        editTextFrom.setText(  DayForm + "-" +  monthForm
                + "-" + yearForm + " " + HourForm+":"+MinuteForm+":"+"00");

        editTextFrom.setFocusable(false);
        editTextFrom.setOnTouchListener(this);
        ViewGroup.LayoutParams  editTextFromparams =  editTextFrom.getLayoutParams();
        editTextFromparams.width  =   widthEditText*3;
        editTextFrom.setLayoutParams(editTextFromparams);
        editTextFrom.addTextChangedListener(new TextWatcher()
        {

            public void afterTextChanged(Editable s)
            {

                ValidationFromAndTo();

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                //  Toast.makeText(context,"beforeTextChanged :"+String.valueOf(s),Toast.LENGTH_SHORT).show();

            }
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                // Toast.makeText(context,"onTextChanged :"+String.valueOf(s),Toast.LENGTH_SHORT).show();

                   if(!MMSItersubmit.isEmpty() && editTextTo.length() != 0 && s.length() != 0 && ValidityDate == true)
                   {
                       ButtonTracking.setEnabled(true);
                   }
                   else
                   {
                       ButtonTracking.setEnabled(false);
                   }
            }
        });

    }

    private void ValidationFromAndTo()
    {
        ValidityDate = false;
        if (editTextFrom.length() != 0 && editTextTo.length() !=0)
        {

            if (SelectedInterval.equalsIgnoreCase("Minute"))
            {
                //SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                Date dFrom = null, dTo = null;
                //String TimeFrom = DayForm+"-"+monthForm+"-"+yearForm+" "+HourForm+":"+MinuteForm+":"+SecondForm;
               // String TimeTo = DayTo+"-"+monthTo+"-"+yearTo+" "+HourTo+":"+MinuteTo+":"+SecondTo;
                try
                {
                    dFrom = df.parse(String.valueOf(editTextFrom.getText()));
                    dTo = df.parse(String.valueOf(editTextTo.getText()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if(dTo.after(dFrom))
                {
                    Calendar calFrom = Calendar.getInstance();
                    calFrom.setTime(dFrom);
                    Calendar calTo= Calendar.getInstance();
                    calTo.setTime(dTo);

                    long diffMillis = Math.abs(calTo.getTimeInMillis()-calFrom.getTimeInMillis());
                    long differenceInDays = TimeUnit.DAYS.convert(diffMillis, TimeUnit.MILLISECONDS);

                    if (diffMillis <= TimeUnit.DAYS.toMillis(1))
                    {

                        ValidityDate = true;
                        if (!MMSItersubmit.isEmpty() && editTextFrom.length() != 0 && editTextTo.length() != 0 && ValidityDate == true)
                        {
                            ButtonTracking.setEnabled(true);
                        } else
                        {
                            ButtonTracking.setEnabled(false);
                        }


                    }
                    else
                    {
                        Toast.makeText(context,"Jarak Waktu maksimal dalam menit adalah satu hari",Toast.LENGTH_LONG).show();
                        if (!MMSItersubmit.isEmpty() && editTextFrom.length() != 0 && editTextTo.length() != 0 && ValidityDate == true)
                        {
                            ButtonTracking.setEnabled(true);
                        } else
                        {
                            ButtonTracking.setEnabled(false);
                        }


                    }
                }
                else
                {
                    Toast.makeText(context,"Data waktu akhir harus lebih baru dari data waktu awal",Toast.LENGTH_LONG).show();
                    if (!MMSItersubmit.isEmpty() && editTextFrom.length() != 0 && editTextTo.length() != 0 && ValidityDate == true)
                    {
                        ButtonTracking.setEnabled(true);
                    } else
                    {
                        ButtonTracking.setEnabled(false);
                    }

                }
            }
            if (SelectedInterval.equalsIgnoreCase("Hour"))
            {

                //SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                Date dFrom = null, dTo = null;
                //String TimeFrom = DayForm+"-"+monthForm+"-"+yearForm+" "+HourForm+":"+MinuteForm+":"+SecondForm;
                // String TimeTo = DayTo+"-"+monthTo+"-"+yearTo+" "+HourTo+":"+MinuteTo+":"+SecondTo;
                try
                {
                    dFrom = df.parse(String.valueOf(editTextFrom.getText()));
                    dTo = df.parse(String.valueOf(editTextTo.getText()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if(dTo.after(dFrom))
                {
                    Calendar calFrom = Calendar.getInstance();
                    calFrom.setTime(dFrom);
                    Calendar calTo= Calendar.getInstance();
                    calTo.setTime(dTo);

                    long diffMillis = Math.abs(calTo.getTimeInMillis() - calFrom.getTimeInMillis());
                    //long differenceInDays = TimeUnit.DAYS.convert(diffMillis, TimeUnit.MILLISECONDS);

                    if (diffMillis <= TimeUnit.DAYS.toMillis(4))
                    {
                        ValidityDate = true;
                        if (!MMSItersubmit.isEmpty() && editTextFrom.length() != 0 && editTextTo.length() != 0 && ValidityDate == true)
                        {
                            ButtonTracking.setEnabled(true);
                        } else
                        {
                            ButtonTracking.setEnabled(false);
                        }

                    }
                    else
                    {
                        Toast.makeText(context,"Jarak Waktu maksimal dalam menit adalah empat hari",Toast.LENGTH_LONG).show();

                        if (!MMSItersubmit.isEmpty() && editTextFrom.length() != 0 && editTextTo.length() != 0 && ValidityDate == true)
                        {
                            ButtonTracking.setEnabled(true);
                        } else
                        {
                            ButtonTracking.setEnabled(false);
                        }
                    }
                }
                else
                {
                    Toast.makeText(context,"Data waktu akhir harus lebih baru dari data waktu awal",Toast.LENGTH_LONG).show();

                    if (!MMSItersubmit.isEmpty() && editTextFrom.length() != 0 && editTextTo.length() != 0 && ValidityDate == true)
                    {
                        ButtonTracking.setEnabled(true);
                    } else
                    {
                        ButtonTracking.setEnabled(false);
                    }

                }
            }

        }

    }
    public void editTextTo()
    {
        editTextTo = (EditText) dialog.findViewById(R.id.editTextTo);

        DateFormat dateFormatyear  = new SimpleDateFormat("yyyy");
        DateFormat dateFormatmonth = new SimpleDateFormat("MM");
        DateFormat dateFormatday   = new SimpleDateFormat("dd");
        DateFormat dateFormathour  = new SimpleDateFormat("HH");
        DateFormat dateFormatmin   = new SimpleDateFormat("mm");

        Date date = new Date();


        yearTo        = String.valueOf(dateFormatyear.format(date));
        if(yearTo.length() == 1)
        {
            yearTo = "0"+yearTo;
        }
        monthTo       = String.valueOf(dateFormatmonth.format(date));
        if (monthTo.length() == 1)
        {
            monthTo = "0"+monthTo;
        }

        DayTo         = String.valueOf(dateFormatday.format(date));
        if(DayTo.length() == 1)
        {
            DayTo = "0"+DayTo;
        }
        HourTo        = String.valueOf(dateFormathour.format(date));
        if(HourTo.length() == 1)
        {
            HourTo   = "0"+HourTo;
        }
        MinuteTo      = String.valueOf(dateFormatmin.format(date));
        if(MinuteTo.length() == 1)
        {
            MinuteTo = "0"+MinuteTo;
        }
        SecondTo      = "59";

        editTextTo.setText(  DayTo + "-" +  monthTo
                + "-" + yearTo + " " + HourTo+":"+MinuteTo+":"+SecondTo);

        editTextTo.setFocusable(false);
        editTextTo.setOnTouchListener(this);
        ViewGroup.LayoutParams  editTextToparams =  editTextTo.getLayoutParams();
        editTextToparams.width  =   widthEditText*3;
        editTextTo.setLayoutParams(editTextToparams);
        editTextTo.addTextChangedListener(new TextWatcher()
        {

            public void afterTextChanged(Editable s)
            {
                ValidationFromAndTo();
                //Toast.makeText(context,"afterTextChanged :"+String.valueOf(s),Toast.LENGTH_SHORT).show();
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                //  Toast.makeText(context,"beforeTextChanged :"+String.valueOf(s),Toast.LENGTH_SHORT).show();

            }
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                // Toast.makeText(context,"onTextChanged :"+String.valueOf(s),Toast.LENGTH_SHORT).show();

                if (!MMSItersubmit.isEmpty() && editTextFrom.length() != 0 && s.length() != 0 && ValidityDate == true)
                {
                    ButtonTracking.setEnabled(true);
                } else
                {
                    ButtonTracking.setEnabled(false);
                }


            }
        });

    }
    public void MMSIeditText ()
    {
         editTextMMSI =(EditText) dialog.findViewById(R.id.editTextMMSI);
         ViewGroup.LayoutParams editTextMMSIparams = editTextMMSI.getLayoutParams();
         editTextMMSIparams.width  =   widthEditText*3;
         editTextMMSI.setLayoutParams(editTextMMSIparams);
         editTextMMSI.addTextChangedListener(new TextWatcher()
         {

             public void afterTextChanged(Editable s)
             {
               //Toast.makeText(context,"afterTextChanged :"+String.valueOf(s),Toast.LENGTH_SHORT).show();
             }
             public void beforeTextChanged(CharSequence s, int start, int count, int after)
             {
               //  Toast.makeText(context,"beforeTextChanged :"+String.valueOf(s),Toast.LENGTH_SHORT).show();
             }
             public void onTextChanged(CharSequence s, int start, int before, int count)
             {
                // Toast.makeText(context,"onTextChanged :"+String.valueOf(s),Toast.LENGTH_SHORT).show();

                 if (s.length()>1 && s.length()<10)
                 {
                     MMSISubmit.clear();
                     NamaKapalSubmit.clear();
                     LayoutParentListViewMMSIsearch.setVisibility(View.VISIBLE);
                     if (CheckLayoutParentListViewMMSIsearch == false)
                     {
                         CheckLayoutParentListViewMMSIsearch = true;
                     }
                     String regex = "[0-9]+";
                     Boolean CheckInput = s.toString().matches(regex);
                     if (CheckInput == true)
                     {
                         SQLiteDatabase dbRead = MapsActivity.DbHelper.getReadableDatabase();
                         String selectQuery = "SELECT * FROM Json_Data_Kapal WHERE MMSI LIKE '%" + s + "%'";
                         Cursor cursor = dbRead.rawQuery(selectQuery, null);

                         if (cursor.moveToFirst())
                         {
                             do
                             {
                                 MMSISubmit.add(cursor.getString(0));
                                 NamaKapalSubmit.add(cursor.getString(1));

                             } while (cursor.moveToNext());

                         }

                         if(  !MMSISubmit.isEmpty() && !NamaKapalSubmit.isEmpty())
                         {
                             LayoutParentListViewMMSIsearch.setVisibility(View.VISIBLE);
                             ListviewKapalMMSI.setAdapter(adapter);
                         }
                     }
                     else
                     {
                         SQLiteDatabase dbRead = MapsActivity.DbHelper.getReadableDatabase();
                         String selectQuery = "SELECT * FROM Json_Data_Kapal WHERE ShipName LIKE '%"+s+"%'";
                         Cursor cursor = dbRead.rawQuery(selectQuery, null);

                         if (cursor.moveToFirst())
                         {
                             do
                             {
                                 MMSISubmit.add(cursor.getString(0));
                                 NamaKapalSubmit.add(cursor.getString(1));
                             } while (cursor.moveToNext());
                         }
                         if(  !MMSISubmit.isEmpty() && !NamaKapalSubmit.isEmpty())
                         {
                             LayoutParentListViewMMSIsearch.setVisibility(View.VISIBLE);
                             ListviewKapalMMSI.setAdapter(adapter);
                         }

                     }
                 }
                 else
                 {
                     if (LayoutParentListViewMMSIsearch.isShown())
                     {
                         LayoutParentListViewMMSIsearch.setVisibility(View.INVISIBLE);
                     }
                 }
             }
         });
    }
    private void CheckDataKapalToTextview()
    {
        if (MMSItersubmit.isEmpty())
        {
            MMSItersubmit.add(Index1);
            KapaldalamListSubmit.add(Index2);
            if(ButtonTracking.isEnabled()==false)
            {
                if (!MMSItersubmit.isEmpty() && ! KapaldalamListSubmit.isEmpty() && editTextFrom.length() != 0 && editTextFrom.length() != 0 && ValidityDate == true)
                {
                    ButtonTracking.setEnabled(true);
                }
                //LayoutStatusKapalTerpilih.setVisibility(View.INVISIBLE);
                //StatusKapalTerpilih.setText("SELECTED VESSELES");
            }

            addKapalToTextview();
        }
        if (!MMSItersubmit.isEmpty())
        {
            for(int i = 0; i<MMSItersubmit.size(); i++)
            {
                if(Index1.equalsIgnoreCase(MMSItersubmit.get(i)))
                {
                    CheckBukanDataKembar = true;
                }
            }
            if(CheckBukanDataKembar == false)
            {
                MMSItersubmit.add(Index1);
                KapaldalamListSubmit.add(Index2);
                addKapalToTextview();
            }
            else
            {
                Toast.makeText(context, "Data telah tersimpan", Toast.LENGTH_LONG).show();
                CheckBukanDataKembar = false;
            }
        }
    }
    private void addKapalToTextview()
    {


        if(Index1 !=null && Index2 != null)
        {
            //Toast.makeText(context,"tes3", Toast.LENGTH_SHORT).show();
            if(KapalSelectSatu.length() == 0)
            {
                LayoutKapalSelectSatu.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                p.addRule(RelativeLayout.BELOW, R.id.LayoutStatusKapalTerpilih);
                p.setMargins(0, 0, 0, 10);
                LayoutKapalSelectSatu.setLayoutParams(p);
                KapalSelectSatu.setText(Index1 + " " + "(" + Index2 + ")");

            }
            else if (KapalSelectDua.length() == 0)
            {
                LayoutKapalSelectDua.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectSatu);
                p.setMargins(0, 0, 0, 10);
                LayoutKapalSelectDua.setLayoutParams(p);
                KapalSelectDua.setText(Index1 + " " +"("+Index2+")");

            }
            else if (KapalSelectTiga.length() == 0)
            {
                LayoutKapalSelectTiga.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectDua);
                p.setMargins(0, 0, 0, 10);
                LayoutKapalSelectTiga.setLayoutParams(p);
                KapalSelectTiga.setText(Index1 + " " +"("+Index2+")");

            }
            else if (KapalSelectEmpat.length() == 0)
            {
                LayoutKapalSelectEmpat.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectTiga);
                p.setMargins(0, 0, 0, 10);
                LayoutKapalSelectEmpat.setLayoutParams(p);
                KapalSelectEmpat.setText(Index1 + " " +"("+Index2+")");

            }
            else if (KapalSelectLima.length() == 0)
            {
                LayoutKapalSelectLima.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectEmpat);
                p.setMargins(0, 0, 0, 10);
                LayoutKapalSelectLima.setLayoutParams(p);
                KapalSelectLima.setText(Index1 + " " +"("+Index2+")");

            }

        }
    }
        @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {

        SelectedMMSI      = MMSISubmit.get(position);
        SelectedNamaKapal = NamaKapalSubmit.get(position);
        editTextMMSI.setText(SelectedMMSI + " " + SelectedNamaKapal);
        ListviewItem = true;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {   switch (v.getId())
        {
            case R.id.editTextFrom:
                if (CheckAlertPickerForm == true)
                {
                    Filter_view_kapal_dynamic_date_from_picker alert = new Filter_view_kapal_dynamic_date_from_picker(context);
                    alert.showDialogDatePicker((Activity)context);
                    CheckAlertPickerForm = false;
                }
                break;
            case R.id.editTextTo:

                if(CheckAlertPickerTo == true)
                {
                    Filter_view_kapal_dynamic_date_to_picker alertTo = new  Filter_view_kapal_dynamic_date_to_picker(context);
                    alertTo.showDialogDatePicker((Activity)context);
                    CheckAlertPickerTo  = false;
                }
                break;
        }
        return false;
    }

    @Override
    public boolean onKey(final DialogInterface dialog, int keyCode, KeyEvent event)
    {

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {

                if(!LayoutParentListViewMMSIsearch.isShown() && CheckLayoutParentListViewMMSIsearch == false )
                {
                    layout_filter_view_kapal_dynamic.startAnimation(animDown);
                    animDown.setAnimationListener(new Animation.AnimationListener()
                    {
                        @Override
                        public void onAnimationStart(Animation animation)
                        {
                            // TODO Auto-generated method stub
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation)
                        {
                            // TODO Auto-generated method stub
                        }

                        @Override
                        public void onAnimationEnd(Animation animation)
                        {
                            Handler handler = new Handler();
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    dialog.dismiss();
                                }
                            });
                            alertNavigation.showDialog((Activity) context);
                        }
                    });
                    return true;
                }
                else if(LayoutParentListViewMMSIsearch.isShown() && CheckLayoutParentListViewMMSIsearch == true)
                {
                    LayoutParentListViewMMSIsearch.setVisibility(View.INVISIBLE);
                    CheckLayoutParentListViewMMSIsearch = false;
                    return true;
                }
        }
        return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {

        SelectedInterval =  SpinnerJenisInterval.getSelectedItem().toString();
        if (SelectedInterval.equalsIgnoreCase("Minute"))
        {
            seekBarInterval.setMax(29);
        }
        if (SelectedInterval.equalsIgnoreCase("Hour"))
        {
            seekBarInterval.setMax(3);
        }
        ValidationFromAndTo();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
    {
        if (SelectedInterval.equalsIgnoreCase("Minute"))
        {
            progressIntervalMinute = progress + 1;
            progressIntervalHour = 0;
            TextviewIntervalValueSeekbar.setText(String.valueOf(progressIntervalMinute));
        }
        if (SelectedInterval.equalsIgnoreCase("Hour"))
        {
            progressIntervalHour = progress + 1;
            progressIntervalMinute = 0;
            TextviewIntervalValueSeekbar.setText(String.valueOf(progressIntervalHour));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar)
    {


    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar)
    {

    }
    @Override
    public void onClick(View v)
    {
        Index1 = null;
        Index2 = null;
        switch (v.getId())
        {
            case R.id.ButtonTracking:
                /*editTextMMSI.setClickable(false);
                ButtonAdd.setClickable(false);
                editTextFrom.setClickable(false);
                editTextTo.setClickable(false);
                SpinnerJenisInterval.setClickable(false);
                seekBarInterval.setClickable(false);
                LayoutTextviewAddKapal.setClickable(false);*/
                MapsActivity.mMap.clear();

                circleProgres.setVisibility(View.VISIBLE);
                if(circleProgres.isShown())
                {
                    mOverlayDialog = new Dialog(context, android.R.style.Theme_Panel); //display an invisible overlay dialog to prevent user interaction and pressing back
                    mOverlayDialog.setCancelable(false);
                    mOverlayDialog.show();
                }

                //MapsActivity.HashmarkerMap.clear();
                //MapsActivity.MarkerList.clear();
                try
                {
                        if(progressIntervalMinute != 0)
                        {
                            Log.e("menit", "menit");
                            String TimeFrom = null,TimeTo = null;
                            String newTimeFrom = null,newTimeTo = null;
                            Date DatenewTimeFrom = null;

                            TimeTo = DayTo+"-"+monthTo+"-"+yearTo+" "+HourTo+":"+MinuteTo+":"+SecondTo;
                            SimpleDateFormat dfTo = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            Date DateTimeTo = dfTo.parse(TimeTo);

                            Date dTo = dfTo.parse(TimeTo);
                            Calendar calTo = Calendar.getInstance();
                            calTo.setTime(dTo);
                            calTo.add(Calendar.MINUTE, -progressIntervalMinute);
                            newTimeTo = dfTo.format(calTo.getTime());
                            Date DatenewTimeTo = dfTo.parse(newTimeTo);

                            int i = 0;
                            Boolean AkhirWaktu = false;
                            SimpleDateFormat dfFrom;
                            do
                            {
                                if (i == 0)
                                {
                                    TimeFrom = DayForm+"-"+monthForm+"-"+yearForm+" "+HourForm+":"+MinuteForm+":"+SecondForm;
                                    dfFrom = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                                    Date dFrom = dfFrom.parse(TimeFrom);
                                    daftarWaktu.add(dFrom);
                                }
                                else
                                {
                                    TimeFrom =  newTimeFrom;
                                    dfFrom = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                                }

                                Date dFrom = dfFrom.parse(TimeFrom);
                                Calendar calFrom = Calendar.getInstance();
                                calFrom.setTime(dFrom);
                                calFrom.add(Calendar.MINUTE, progressIntervalMinute);
                                newTimeFrom = dfFrom.format(calFrom.getTime());
                                DatenewTimeFrom = dfFrom.parse(newTimeFrom);
                                daftarWaktu.add(DatenewTimeFrom);

                                if (DatenewTimeFrom.equals(DateTimeTo))
                                {
                                    AkhirWaktu = true;

                                }
                                else if (DatenewTimeFrom.after(DatenewTimeTo))
                                {
                                    AkhirWaktu = true;
                                }
                                i++;

                            }
                            while( AkhirWaktu == false);

                        }
                        if(progressIntervalHour != 0)
                        {

                            Log.e("Jam", "Jam");
                            String TimeFrom = null,TimeTo = null;
                            String newTimeFrom = null,newTimeTo = null;
                            Date DatenewTimeFrom = null;

                            TimeTo = DayTo+"-"+monthTo+"-"+yearTo+" "+HourTo+":"+MinuteTo+":"+SecondTo;
                            SimpleDateFormat dfTo = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            Date DateTimeTo = dfTo.parse(TimeTo);
                            Date dTo = dfTo.parse(TimeTo);
                            Calendar calTo = Calendar.getInstance();
                            calTo.setTime(dTo);
                            calTo.add(Calendar.HOUR, -progressIntervalHour);
                            newTimeTo = dfTo.format(calTo.getTime());
                            Date DatenewTimeTo = dfTo.parse(newTimeTo);


                            int i = 0;
                            Boolean AkhirWaktu = false;
                            SimpleDateFormat dfFrom;
                            Date dFrom;
                            Calendar calFrom;
                            do
                            {
                                if (i == 0)
                                {

                                    TimeFrom = DayForm+"-"+monthForm+"-"+yearForm+" "+HourForm+":"+MinuteForm+":"+SecondForm;
                                    dfFrom = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                                     dFrom = dfFrom.parse(TimeFrom);
                                    daftarWaktu.add(dFrom);

                                }
                                else
                                {
                                    TimeFrom =  newTimeFrom;
                                    dfFrom = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                                }

                                dFrom = dfFrom.parse(TimeFrom);
                                calFrom = Calendar.getInstance();
                                calFrom.setTime(dFrom);
                                calFrom.add(Calendar.HOUR, progressIntervalHour);
                                newTimeFrom = dfFrom.format(calFrom.getTime());
                                DatenewTimeFrom = dfFrom.parse(newTimeFrom);
                                daftarWaktu.add(DatenewTimeFrom);


                                if (DatenewTimeFrom.equals(DateTimeTo))
                                {
                                    AkhirWaktu = true;
                                    //Toast.makeText(context,AkhirWaktu.toString(),Toast.LENGTH_LONG).show();

                                }
                                else if (DatenewTimeFrom.after(DatenewTimeTo))
                                {
                                    AkhirWaktu = true;
                                   // Toast.makeText(context,AkhirWaktu.toString(),Toast.LENGTH_LONG).show();

                                }
                                i++;
                            }
                            while(AkhirWaktu == false);
                        }
                        getJsonRecorded();
                }
                catch (ParseException e)
                {
                    e.printStackTrace();
                    Toast.makeText(context,e.toString(),Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.buttonAdd:

                if(MMSItersubmit.size()<=4)
                {
                    if (ListviewItem == true)
                    {

                        SQLiteDatabase dbRead = MapsActivity.DbHelper.getReadableDatabase();
                        String selectQuery = "SELECT * FROM Json_Data_Kapal WHERE MMSI LIKE '%" +  SelectedMMSI + "%'";
                        Cursor cursor = dbRead.rawQuery(selectQuery, null);
                        if (cursor.moveToFirst())
                        {
                            do
                            {
                                Index1 = cursor.getString(0);
                                Index2 = cursor.getString(1);


                            } while (cursor.moveToNext());
                        }

                        CheckDataKapalToTextview();
                        ListviewItem = false;

                    }
                    else if(ListviewItem == false)
                    {
                        String[] InputEditText = String.valueOf(editTextMMSI.getText()).split("\\s+");

                        SQLiteDatabase dbRead = MapsActivity.DbHelper.getReadableDatabase();
                        String selectQuery = "SELECT * FROM Json_Data_Kapal WHERE MMSI= '"+InputEditText[0]+ "'";
                        Cursor cursor = dbRead.rawQuery(selectQuery, null);
                        if (cursor.getCount() == 0)
                        {
                            selectQuery = "SELECT * FROM Json_Data_Kapal WHERE ShipName= '"+InputEditText[0].toUpperCase()+ "'";
                            cursor = dbRead.rawQuery(selectQuery, null);
                            if(cursor.getCount() != 0)
                            {
                                if (cursor.moveToFirst())
                                {
                                    do
                                    {
                                        Index1 = cursor.getString(0);
                                        Index2 = cursor.getString(1);

                                    } while (cursor.moveToNext());
                                }
                               CheckDataKapalToTextview();
                            }
                            else
                            {
                                selectQuery = "SELECT * FROM Json_Data_Kapal WHERE ShipName= '"+ InputEditText[0].toLowerCase()+ "'";
                                cursor = dbRead.rawQuery(selectQuery, null);
                                if(cursor.getCount() != 0)
                                {
                                    if (cursor.moveToFirst())
                                    {
                                        do
                                        {
                                            Index1 = cursor.getString(0);
                                            Index2 = cursor.getString(1);

                                        } while (cursor.moveToNext());
                                    }
                                    CheckDataKapalToTextview();
                                }
                                else
                                {
                                    Toast.makeText(context,"Data tidak ditemukan", Toast.LENGTH_SHORT).show();
                                }
                            }

                        }
                        else if(cursor.getCount() != 0)
                        {

                            if (cursor.moveToFirst())
                            {
                                do
                                {
                                    Index1 = cursor.getString(0);
                                    Index2 = cursor.getString(1);


                                } while (cursor.moveToNext());
                            }
                            CheckDataKapalToTextview();

                        }

                    }

                    editTextMMSI.setText(null);
                    CheckLayoutParentListViewMMSIsearch = false;

                }
                else
                {
                    Toast.makeText(context,"Jumlah kapal maksimal yang bisa ditampilkan adalah 5", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.buttonDeleteKapalSatu:
                if (!MMSItersubmit.isEmpty() && !KapaldalamListSubmit.isEmpty())
                {
                    MMSItersubmit.remove(0);
                    KapaldalamListSubmit.remove(0);

                    if(MMSItersubmit.isEmpty() )
                    {
                        KapalSelectSatu.setText("");
                        LayoutKapalSelectSatu.setVisibility(View.INVISIBLE);
                        if(MMSItersubmit.isEmpty() || editTextFrom.length() == 0 || editTextFrom.length() == 0 || ValidityDate == false)
                        {
                            ButtonTracking.setEnabled(false);
                        }
                    }
                    if(MMSItersubmit.size() == 1)
                    {
                        KapalSelectSatu.setText(MMSItersubmit.get(0)+ " " + KapaldalamListSubmit.get(0));
                        KapalSelectDua.setText ("");
                        LayoutKapalSelectDua.setVisibility(View.INVISIBLE);
                        if (!LayoutKapalSelectDua.isShown())
                        {

                            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            p.addRule(RelativeLayout.ALIGN_LEFT, R.id.LayoutKapalSelectSatu);
                            p.setMargins(0, 0, 0, 10);
                            LayoutKapalSelectEmpat.setLayoutParams(p);
                            LayoutKapalSelectLima.setLayoutParams(p);
                            LayoutKapalSelectTiga.setLayoutParams(p);
                            LayoutKapalSelectDua.setLayoutParams(p);
                        }
                    }
                    else if(MMSItersubmit.size() == 2)
                    {
                        KapalSelectSatu.setText(MMSItersubmit.get(0)+ " " + KapaldalamListSubmit.get(0));
                        KapalSelectDua.setText(MMSItersubmit.get(1)+ " " + KapaldalamListSubmit.get(1));
                        KapalSelectTiga.setText ("");
                        LayoutKapalSelectTiga.setVisibility(View.INVISIBLE);
                        if (!LayoutKapalSelectTiga.isShown())
                        {

                            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectSatu);
                            p.setMargins(0, 0, 0, 10);
                            LayoutKapalSelectEmpat.setLayoutParams(p);
                            LayoutKapalSelectLima.setLayoutParams(p);
                            LayoutKapalSelectTiga.setLayoutParams(p);
                        }
                    }
                    else  if(MMSItersubmit.size() == 3)
                    {
                        KapalSelectSatu.setText(MMSItersubmit.get(0)+ " " + KapaldalamListSubmit.get(0));
                        KapalSelectDua.setText(MMSItersubmit.get(1)+ " " + KapaldalamListSubmit.get(1));
                        KapalSelectTiga.setText(MMSItersubmit.get(2)+ " " + KapaldalamListSubmit.get(2));
                        KapalSelectEmpat.setText ("");
                        LayoutKapalSelectEmpat.setVisibility(View.INVISIBLE);
                        if (!LayoutKapalSelectEmpat.isShown())
                        {
                            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectDua);
                            p.setMargins(0, 0, 0, 10);
                            LayoutKapalSelectEmpat.setLayoutParams(p);
                            LayoutKapalSelectLima.setLayoutParams(p);
                        }
                    }
                    else  if(MMSItersubmit.size() == 4)
                    {
                        KapalSelectSatu.setText(MMSItersubmit.get(0)+ " " + KapaldalamListSubmit.get(0));
                        KapalSelectDua.setText(MMSItersubmit.get(1)+ " " + KapaldalamListSubmit.get(1));
                        KapalSelectTiga.setText(MMSItersubmit.get(2)+ " " + KapaldalamListSubmit.get(2));
                        KapalSelectEmpat.setText(MMSItersubmit.get(3)+ " " + KapaldalamListSubmit.get(3));
                        KapalSelectLima.setText ("");
                        LayoutKapalSelectLima.setVisibility(View.INVISIBLE);
                        if (!LayoutKapalSelectLima.isShown())
                        {
                            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectTiga);
                            p.setMargins(0, 0, 0, 10);
                            LayoutKapalSelectLima.setLayoutParams(p);

                        }
                    }
                }
                break;
            case R.id.buttonDeleteKapalDua:

                MMSItersubmit.remove(1);
                KapaldalamListSubmit.remove(1);

                if(MMSItersubmit.size() == 1)
                {
                    KapalSelectDua.setText ("");
                    LayoutKapalSelectDua.setVisibility(View.INVISIBLE);
                    if (!LayoutKapalSelectDua.isShown())
                    {
                        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        p.addRule(RelativeLayout.ALIGN_LEFT, R.id.LayoutKapalSelectSatu);
                        p.setMargins(0, 0, 0, 10);
                        LayoutKapalSelectEmpat.setLayoutParams(p);
                        LayoutKapalSelectLima.setLayoutParams(p);
                        LayoutKapalSelectTiga.setLayoutParams(p);
                        LayoutKapalSelectDua.setLayoutParams(p);
                    }
                }
                else if(MMSItersubmit.size() == 2)
                {
                    KapalSelectDua.setText(MMSItersubmit.get(1)+ " " + KapaldalamListSubmit.get(1));
                    KapalSelectTiga.setText ("");
                    LayoutKapalSelectTiga.setVisibility(View.INVISIBLE);
                    if (!LayoutKapalSelectTiga.isShown())
                    {
                        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectSatu);
                        p.setMargins(0, 0, 0, 10);
                        LayoutKapalSelectEmpat.setLayoutParams(p);
                        LayoutKapalSelectLima.setLayoutParams(p);
                        LayoutKapalSelectTiga.setLayoutParams(p);
                    }
                }
                else  if(MMSItersubmit.size() == 3)
                {
                    KapalSelectDua.setText(MMSItersubmit.get(1)+ " " + KapaldalamListSubmit.get(1));
                    KapalSelectTiga.setText(MMSItersubmit.get(2)+ " " + KapaldalamListSubmit.get(2));
                    KapalSelectEmpat.setText ("");
                    LayoutKapalSelectEmpat.setVisibility(View.INVISIBLE);
                    if (!LayoutKapalSelectEmpat.isShown())
                    {
                        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectDua);
                        p.setMargins(0, 0, 0, 10);
                        LayoutKapalSelectEmpat.setLayoutParams(p);
                        LayoutKapalSelectLima.setLayoutParams(p);
                    }
                }
                else  if(MMSItersubmit.size() == 4)
                {
                    KapalSelectDua.setText(MMSItersubmit.get(1)+ " " + KapaldalamListSubmit.get(1));
                    KapalSelectTiga.setText(MMSItersubmit.get(2)+ " " + KapaldalamListSubmit.get(2));
                    KapalSelectEmpat.setText(MMSItersubmit.get(3)+ " " + KapaldalamListSubmit.get(3));
                    KapalSelectLima.setText ("");
                    LayoutKapalSelectLima.setVisibility(View.INVISIBLE);
                    if (!LayoutKapalSelectLima.isShown())
                    {
                        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectTiga);
                        p.setMargins(0, 0, 0, 10);
                        LayoutKapalSelectLima.setLayoutParams(p);
                    }
                }

                break;
            case R.id.buttonDeleteKapalTiga:

                MMSItersubmit.remove(2);
                KapaldalamListSubmit.remove(2);

                if(MMSItersubmit.size() == 2)
                {

                    KapalSelectTiga.setText ("");
                    LayoutKapalSelectTiga.setVisibility(View.INVISIBLE);
                    if (!LayoutKapalSelectTiga.isShown())
                    {

                        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectSatu);
                        p.setMargins(0, 0, 0, 10);
                        LayoutKapalSelectEmpat.setLayoutParams(p);
                        LayoutKapalSelectLima.setLayoutParams(p);
                        LayoutKapalSelectTiga.setLayoutParams(p);

                    }
                }
                else  if(MMSItersubmit.size() == 3)
                {

                    KapalSelectTiga.setText(MMSItersubmit.get(2)+ " " + KapaldalamListSubmit.get(2));
                    KapalSelectEmpat.setText ("");
                    LayoutKapalSelectEmpat.setVisibility(View.INVISIBLE);
                    if (!LayoutKapalSelectEmpat.isShown())
                    {

                        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectDua);
                        p.setMargins(0, 0, 0, 10);
                        LayoutKapalSelectEmpat.setLayoutParams(p);
                        LayoutKapalSelectLima.setLayoutParams(p);

                    }
                }
                else  if(MMSItersubmit.size() == 4)
                {

                    KapalSelectTiga.setText(MMSItersubmit.get(2)+ " " + KapaldalamListSubmit.get(2));
                    KapalSelectEmpat.setText(MMSItersubmit.get(3)+ " " + KapaldalamListSubmit.get(3));
                    KapalSelectLima.setText ("");
                    LayoutKapalSelectLima.setVisibility(View.INVISIBLE);
                    if (!LayoutKapalSelectLima.isShown())
                    {
                        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectTiga);
                        p.setMargins(0, 0, 0, 10);
                        LayoutKapalSelectLima.setLayoutParams(p);
                    }
                }
                break;
            case R.id.buttonDeleteKapalEmpat:
                MMSItersubmit.remove(3);
                KapaldalamListSubmit.remove(3);

                if(MMSItersubmit.size() == 3)
                {
                    KapalSelectEmpat.setText ("");
                    LayoutKapalSelectEmpat.setVisibility(View.INVISIBLE);

                    if (!LayoutKapalSelectEmpat.isShown())
                    {
                        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectDua);
                        p.setMargins(0, 0, 0, 10);
                        LayoutKapalSelectEmpat.setLayoutParams(p);
                        LayoutKapalSelectLima.setLayoutParams(p);
                    }
                }
                else  if(MMSItersubmit.size() == 4)
                {

                    KapalSelectEmpat.setText(MMSItersubmit.get(3)+ " " + KapaldalamListSubmit.get(3));
                    KapalSelectLima.setText ("");
                    LayoutKapalSelectLima.setVisibility(View.INVISIBLE);

                    if (!LayoutKapalSelectLima.isShown())
                    {
                        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectTiga);
                        p.setMargins(0, 0, 0, 10);
                        LayoutKapalSelectLima.setLayoutParams(p);
                    }
                }

                break;
            case R.id.buttonDeleteKapalLima:
                MMSItersubmit.remove(4);
                KapaldalamListSubmit.remove(4);
                KapalSelectLima.setText("");
                LayoutKapalSelectLima.setVisibility(View.INVISIBLE);

                if (!LayoutKapalSelectLima.isShown())
                {
                    RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    p.addRule(RelativeLayout.BELOW, R.id.LayoutKapalSelectTiga);
                    p.setMargins(0, 0, 0, 10);
                    LayoutKapalSelectLima.setLayoutParams(p);
                }
                break;
        }
    }

    public void getJsonRecorded()
    {
        int y;
        SimpleDateFormat df;
        String URL;
        String dateFrom_params = null, dateTo_params = null;
        StringRequest req = null;
        for (int i = 0; i<MMSItersubmit.size(); i++)
        {
            MMSItersubmitParam = i;
            mmsi_params = MMSItersubmit.get(i);
            for (int x = 0; x<daftarWaktu.size(); x++)
            {
                if(x != daftarWaktu.size()-1)
                {
                    df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    dateFrom_params = String.valueOf(df.format(daftarWaktu.get(x)));
                    dateFrom_params = dateFrom_params.replaceAll("\\s", "%20");
                    y = x+1;
                    dateTo_params = String.valueOf(df.format(daftarWaktu.get(y)));
                    dateTo_params = dateTo_params.replaceAll("\\s", "%20");
                    URL = "http://27.111.46.52/modabile.dlu/api/vessel/getrecorded/?mmsi="+mmsi_params+"&dateFrom="+dateFrom_params+"&dateTo="+dateTo_params+"";

                     req  = new StringRequest(Request.Method.GET,URL,
                            new Response.Listener<String>()
                            {
                                @Override
                                public void onResponse(String response)
                                {
                                    if(response != null)
                                    {
                                        responeJumlah = responeJumlah+1;
                                        try
                                        {
                                            jsonResult = new JSONObject(response);
                                            JsonArray  = jsonResult.getJSONArray("features");
                                           if(JsonArray.length() != 0)
                                            {

                                                ArrayListJson.add(JsonArray);
                                            }
                                            if (responeJumlah == daftarWaktu.size()*MMSItersubmit.size()-MMSItersubmit.size())
                                           {
                                                gambarKapal();
                                                circleProgres.setVisibility(View.INVISIBLE);
                                                if(!circleProgres.isShown())
                                                {
                                                    mOverlayDialog.dismiss();
                                                }
                                                MapsActivity.AlertDialogDaftarKapal = false;
                                                MMSItersubmit.clear();
                                                KapaldalamListSubmit.clear();
                                                if (!circleProgres.isShown())
                                                {
                                                    Handler handler = new Handler();
                                                    handler.post(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            dialog.dismiss();
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                        catch (JSONException e)
                                        {
                                            e.printStackTrace();
                                        }
                                    }

                                }
                            },
                            new Response.ErrorListener()
                            {
                                @Override
                                public void onErrorResponse(VolleyError error)
                                {
                                    Log.e("Error", error.toString());
                                }
                            })

                    {
                    };
                    int socketTimeout = 10000;
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    req.setRetryPolicy(policy);
                    VolleySingelton.getInstance(context).addToRequestQueue(req);
                }
            }

        }

    }
    private void gambarKapal()
    {
        //points.clear();
        String MMSI,ShipName,StringLatitudeJson,StringLongitudeJson,TimeStampReceiver,ToStern,ROT,
                ShipType,IMONumber,DangerHead,Draught,ETAMinute,ETADay,NavStatus,StationID,CallSign,
                ZoneID,COG,HDG,ETAHour,StationName,ZoneName,Grt,ToPort,ETAMonth,SOG,DangerDist,ToBow,
                GrtBySiuk,ToStarBoard,NavStatusDescription,Destination,type,ShipTypeDescription;
        LatLng Kordinat;
        JSONArray coordinates;
        Double  LatitudeJson, LongitudeJson;
        JSONObject Feature,geometry,properties;
        JSONArray jsonArray;
        String KeteranganPage;
      //  String TitikAwalMarkerID = null,TitikAkhirMarkerID = null;
        try
        {
            for (int w = 0; w<ArrayListJson.size(); w++)
            {
               jsonArray = ArrayListJson.get(w);
                for (int j = 0; j <jsonArray.length(); j++)
                {
                    Feature                      = (JSONObject) jsonArray.get(j);
                    geometry                     = (JSONObject) Feature.get("geometry");
                    properties                   = (JSONObject) Feature.get("properties");
                    TimeStampReceiver            = properties.optString("TimeStampReceiver");
                    ToStern                      = properties.optString("ToStern");
                    ROT                          = properties.optString("ROT");
                    ShipType                     = properties.optString("ShipType");
                    IMONumber                    = properties.optString("IMONumber");
                    DangerHead                   = properties.optString("DangerHead");
                    Draught              = properties.optString("Draught");
                    ETAMinute            = properties.optString("ETAMinute");
                    ETADay               = properties.optString("ETADay");
                    NavStatus            = properties.optString("NavStatus");
                    StationID            = properties.optString("StationID");
                    CallSign             = properties.optString("CallSign");
                    ZoneID               = properties.optString("ZoneID");
                    COG                  = properties.optString("COG");
                    HDG                  = properties.optString("HDG");
                    ETAHour              = properties.optString("ETAHour");
                    StationName          = properties.optString("StationName");
                    ZoneName             = properties.optString("ZoneName");
                    Grt                  = properties.optString("Grt");
                    ToPort               = properties.optString("ToPort");
                    ETAMonth             = properties.optString("ETAMonth");
                    SOG                  = properties.optString("SOG");
                    DangerDist           = properties.optString("DangerDist");
                    ToBow                = properties.optString("ToBow");
                    GrtBySiuk            = properties.optString("GrtBySiuk");
                    ToStarBoard          = properties.optString("ToStarBoard");
                    NavStatusDescription = properties.optString("NavStatusDescription");
                    Destination          = properties.optString("Destination");
                    MMSI                 = properties.optString("MMSI");
                    ShipTypeDescription  = properties.optString("ShipTypeDescription");
                    ShipName             = properties.optString("ShipName");
                    type                 = geometry.optString("type");
                    coordinates          = (JSONArray) geometry.get("coordinates");
                    StringLatitudeJson   = String.valueOf(coordinates.get(1));
                    StringLongitudeJson  = String.valueOf(coordinates.get(0));;

                    LatitudeJson         = Double.valueOf(StringLatitudeJson);
                    LongitudeJson        = Double.valueOf(StringLongitudeJson);
                    Kordinat             = new LatLng(LatitudeJson,LongitudeJson);

                    if(MMSIPerbandingan == null)
                    {
                        MapsActivity.markerName  = MapsActivity.mMap.addMarker(new MarkerOptions().position(Kordinat).icon(BitmapDescriptorFactory.fromResource(R.drawable.kargo_titik)).title(ShipName));
                        MapsActivity.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Kordinat, 5));
                        CameraAwal = true;
                        //TitikAwalMarkerID        = MapsActivity.markerName.getId();
                        MMSIPerbandingan         = MMSI;
                        points.add(Kordinat);
                        awalKapal=true;
                    }
                    else
                    {
                        if(MMSIPerbandingan.equalsIgnoreCase(MMSI))
                        {
                            if(awalKapal == true)
                            {
                                MapsActivity.markerName.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.awal_kapal));
                                awalKapal = false;
                            }
                            else
                            {
                                MapsActivity.markerName.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.jalur_kapal));
                            }

                            MapsActivity.markerName  = MapsActivity.mMap.addMarker(new MarkerOptions().position(Kordinat).icon(BitmapDescriptorFactory.fromResource(R.drawable.kargo_titik)).title(ShipName));
                            points.add(Kordinat);

                            /*if (w == ArrayListJson.size()-1)
                            {
                                TitikAkhirMarkerID   = MapsActivity.markerName.getId();
                            }*/
                        }
                        else
                        {

                            //TitikAkhirMarkerID       = MapsActivity.markerName.getId();
                            awalKapal                = true;
                            MMSIPerbandingan         = MMSI;
                            MapsActivity.markerName  = MapsActivity.mMap.addMarker(new MarkerOptions().position(Kordinat).icon(BitmapDescriptorFactory.fromResource(R.drawable.kargo_titik)).title(ShipName));
                            //TitikAwalMarkerID        = MapsActivity.markerName.getId();
                            points.clear();
                            points.add(Kordinat);
                        }
                    }

                    KeteranganPage = "Detail";
                    List<String> values = new ArrayList<>();
                    values.add(TimeStampReceiver);
                    values.add(ToStern);
                    values.add(ROT);
                    values.add(ShipType);
                    values.add(IMONumber);
                    values.add(DangerHead);
                    values.add(Draught);
                    values.add(ETAMinute);
                    values.add(ETADay);
                    values.add(NavStatus);
                    values.add(StationID);
                    values.add(CallSign);
                    values.add(ZoneID);
                    values.add(COG);
                    values.add(HDG);
                    values.add(ETAHour);
                    values.add(StationName);
                    values.add(ZoneName);
                    values.add(Grt);
                    values.add(ToPort);
                    values.add(ETAMonth);
                    values.add(SOG);
                    values.add(DangerDist);
                    values.add(ToBow);
                    values.add(GrtBySiuk);
                    values.add(ToStarBoard);
                    values.add(NavStatusDescription);
                    values.add(Destination);
                    values.add(MMSI);
                    values.add(ShipTypeDescription);
                    values.add(ShipName);
                    values.add(type);
                    values.add(StringLatitudeJson);
                    values.add(StringLongitudeJson);
                    values.add(KeteranganPage);
                    values.add(KeteranganPage);
                    MapsActivity.HashmarkerMap.put(MapsActivity.markerName.getId(), values);
                    MapsActivity.MarkerList.add(MapsActivity.markerName);

                    polylinedraw();
                    if(points.size()==2)
                    {
                        points.remove(0);
                    }
                   // TitikAwalMarkerID = null;
                   // TitikAkhirMarkerID = null;
                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void polylinedraw()
    {
        PolylineOptions options = new PolylineOptions().width(2).color(Color.BLUE).geodesic(true);
        for (int i = 0; i < points.size(); i++)
        {
            LatLng point = points.get(i);
            options.add(point);
        }
        line = MapsActivity.mMap.addPolyline(options);

    }



}
