package mapsship.project.mapsship;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;


import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MapsActivity extends AppCompatActivity implements View.OnClickListener ,
        View.OnTouchListener,GoogleMap.OnMarkerClickListener ,GoogleMap.InfoWindowAdapter,SearchView.OnQueryTextListener,
        ListView.OnItemClickListener, GoogleMap.OnMapClickListener
{

    public static GoogleMap mMap; // Might be null if Google Play services APK is not available.
    ArrayList<LatLng> points = new ArrayList<LatLng>();
    public static ArrayList<Marker> MarkerList = new ArrayList<Marker>();
    LatLng KordinatKapal;
    int index,indexCameraClick,PercobaanAmbilData = 0;;
    public static int width,height;
    Polyline line;
    private Timer timer;
    public static Marker markerName;
    float startY = 0;
    Animation animUp, animDown,animUpToolbar, animDownToolbar,animUp_layout_parent_searchresult,
            animDown_layout_parent_searchresult, animUp_layout_head_parent_searchresult,
            animDown_layout_head_parent_searchresult,Searchview_Effect, flow_button_visible,flow_button_invisible;
    Toolbar toolbar;
    Button NextButton,BackButton,DetailInfo,Toolbar_Reload_Button;

    Boolean checkGestureMaps = true,durasiNotificationFirstTime = false,CameraAwal = false,timerdownload = false;
    RelativeLayout ll,Layout_Toolbar_Reload_Button;
    public static Map<String, List<String>> HashmarkerMap = new HashMap<String, List<String>>();
    String TimeStampReceiverHashValue,ToSternHashValue,ROTHashValue,ShipTypeHashValue,IMONumberHashValue,
            DangerHeadHashValue,DraughtHashValue,ETAMinuteHashValue,ETADayHashValue,NavStatusHashValue,
            StationIDHashValue,CallSignHashValue,ZoneIDHashValue,COGHashValue,HDGHashValue,ETAHourHashValue,
            StationNameHashValue,ZoneNameHashValue,GrtHashValue,ToPortHashValue,ETAMonthHashValue,SOGHashValue,
            DangerDistHashValue,ToBowHashValue,GrtBySiukHashValue,ToStarBoardHashValue,NavStatusDescriptionHashValue,
            DestinationHashValue,MMSIHashValue,ShipTypeDescription,ShipNameHashValue,TypeHashValue,StringLatitudeJsonHashValue,
            StringLongitudeJsonHashValue,TitikAwalMarkerIDHashValue,TitikAkhirMarkerIDnHashValue,
            PosisiMarkerSekarang;
    public  static String  JsonData;
    TextView Namakapal,TanggalKapal,JamKapal,KecepatanKapal,textTimer;
    SharedPreferences prefs = null;
    ProgressBar circleProgres;
    CountDownTimer durasi;
    boolean ReloadPertamaKali = true;
    RelativeLayout layout_parent_searchresult,layout_head_parent_searchresult;
    boolean StatusChangeBackButton =  false;
    SearchView searchView;
    TextView data_tidak_ada;
    public static DbHelper  DbHelper;
    SQLiteDatabase dbWrite;
    ContentValues values;
    ListView ListviewSearchResult;
    ArrayList<String> SearchKey = new ArrayList<String>();
    ArrayList<String>  SearchKeyName = new ArrayList<String>();
    ArrayList<String>  LatSearch = new ArrayList<String>();
    ArrayList<String>  LongSearch = new ArrayList<String>();
    ArrayList<String>  ListNamaKapal = new ArrayList<String>();
    ArrayList<String>  ListMMSI = new ArrayList<String>();
    ArrayList<String>  ListTimeStamp = new ArrayList<String>();
    ArrayList<String>  LatKapal = new ArrayList<String>();
    ArrayList<String>  LongtKapal = new ArrayList<String>();
    RelativeLayout layout_parent_search;
    Info_Detail_Kapal alertKapalDetail;
    Navigation_page alertNavigation;
    Button button_listkapal;
    public static Boolean AlertDialogDaftarKapal = false;
    public static Boolean AlertDialogInfoKapalDetail = false;
    FloatingActionButton floatingActionButton;
    RelativeLayout layout_parent_maps;
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);


        Log.e("Android", "onCreate() event");
        prefs = getSharedPreferences("mapsship.project.mapsship", MODE_PRIVATE);
        DbHelper = new DbHelper(getBaseContext());
        values = new ContentValues();
        Log.e("THREAD JUMLAH", String.valueOf(java.lang.Thread.activeCount()));
        alertKapalDetail = new Info_Detail_Kapal(getBaseContext());
        alertNavigation = new Navigation_page(MapsActivity.this, ListNamaKapal, ListMMSI,
              ListTimeStamp, LatKapal,  LongtKapal);

        SQLiteDatabase dbRead = DbHelper.getReadableDatabase();
        String selectQuery = "SELECT*FROM " + DbHelper.TABLE_NAME;
        Cursor cursor = dbRead.rawQuery(selectQuery, null);

        if (cursor.getCount() != 0)
        {
            SQLiteDatabase dbWrite = DbHelper.getWritableDatabase();
            dbWrite.delete(DbHelper.TABLE_NAME,null,null);

        }

        circleProgres = (ProgressBar) findViewById(R.id.circleProgres);

        textTimer  = (TextView) findViewById(R.id.textTimer);

        textTimer.setVisibility(View.INVISIBLE);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(this);
        flow_button_visible   = AnimationUtils.loadAnimation(this, R.anim.flow_button_visible);
        flow_button_invisible   = AnimationUtils.loadAnimation(this, R.anim.flow_button_invisible);
       // animDown_layout_parent_searchresult = AnimationUtils.loadAnimation(this, R.anim.anim_down_layout_parent_searchresult);




        NextButton = (Button) findViewById(R.id.next_button);
        NextButton.setOnClickListener(this);

        BackButton = (Button) findViewById(R.id.back_button);
        BackButton.setOnClickListener(this);

        DetailInfo = (Button) findViewById(R.id.DetailInfo);
        DetailInfo.setOnClickListener(this);
        DisplayMetrics displaymetrics = new DisplayMetrics();

        layout_parent_search = (RelativeLayout) findViewById(R.id.layout_parent_search);

        Toolbar_Reload_Button = (Button) findViewById(R.id.Toolbar_Reload_Button);
        Toolbar_Reload_Button.setOnClickListener(this);
        // Toolbar_Reload_Button.setVisibility(View.GONE);

        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width  = displaymetrics.widthPixels;
        height = displaymetrics.heightPixels;


        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        searchView = (SearchView) findViewById(R.id.searchView);
        ViewGroup.LayoutParams searchViewparams = searchView.getLayoutParams();
        searchViewparams.width  = (width/2)+115;
        searchView.setLayoutParams(searchViewparams);

        searchView.setOnTouchListener(this);
        searchView.setOnClickListener(this);
        searchView.setOnQueryTextListener(this);
        searchView.setIconified(false);
        searchView.setIconifiedByDefault(false);
        searchView.setFocusable(false);
        searchView.setFocusableInTouchMode(false);
        searchView.clearFocus();


        int id = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) searchView.findViewById(id);
        textView.setTextColor(Color.GRAY);
        textView.setHintTextColor(Color.GRAY);

        int crossId = searchView.getContext().getResources().getIdentifier("android:id/search_close_btn", null, null);            // Getting the 'search_plate' LinearLayout.
        ImageView image = (ImageView) searchView.findViewById(crossId);
        Drawable d = image.getDrawable();
        d.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);


        int searchImgId = searchView.getContext().getResources().getIdentifier("android:id/search_mag_icon", null, null);
        ImageView searchImage = (ImageView) searchView.findViewById(searchImgId);
        Drawable searchImageDrawable = searchImage.getDrawable();
        //searchImage.setBackgroundResource(R.drawable.arrow_back);
        searchImageDrawable.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        //searchImage.setVisibility(View.GONE);



        setUpMapIfNeeded();
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnMarkerClickListener(this);
        mMap.setInfoWindowAdapter(this);
        mMap.setOnMapClickListener(this);


        CheckFirstTime();
        // timer();

        layout_parent_searchresult = (RelativeLayout) findViewById(R.id.layout_parent_searchresult);
        animUp_layout_parent_searchresult   = AnimationUtils.loadAnimation(this, R.anim.anim_up_layout_parent_searchresult);
        animDown_layout_parent_searchresult = AnimationUtils.loadAnimation(this, R.anim.anim_down_layout_parent_searchresult);

        layout_head_parent_searchresult = (RelativeLayout) findViewById(R.id.layout_head_parent_searchresult);
        animUp_layout_head_parent_searchresult   = AnimationUtils.loadAnimation(this, R.anim.anim_up_layout_head_parent_searchresult);
        animDown_layout_head_parent_searchresult = AnimationUtils.loadAnimation(this, R.anim.anim_down_layout_head_parent_searchresult);

        Searchview_Effect = AnimationUtils.loadAnimation(this, R.anim.searchview_effect);

        ll = (RelativeLayout) findViewById(R.id.slider);
        ll.setVisibility(View.INVISIBLE);



        animUp   = AnimationUtils.loadAnimation(this, R.anim.anim_up);
        animDown = AnimationUtils.loadAnimation(this, R.anim.anim_down);


        Layout_Toolbar_Reload_Button = (RelativeLayout) findViewById(R.id.Layout_Toolbar_Reload_Button);
        Layout_Toolbar_Reload_Button.setVisibility(View.INVISIBLE);

        animUpToolbar   = AnimationUtils.loadAnimation(this, R.anim.anim_up_toolbar);
        animDownToolbar = AnimationUtils.loadAnimation(this, R.anim.anim_down_toolbar);

        toolbar  = (Toolbar) findViewById(R.id.my_toolbar);
        toolbar.setOnTouchListener(this);
        data_tidak_ada = (TextView) findViewById(R.id.data_tidak_ada);

       // Layout_Textview_data_tidak_ada = (RelativeLayout) findViewById(R.id.Layout_Textview_data_tidak_ada);

        ListviewSearchResult = (ListView) findViewById(R.id.listview_search_result);



        button_listkapal = (Button)findViewById(R.id.button_listkapal);
       //button_listkapal.setBackground(R.drawable.ic_navigation_drawer);
        button_listkapal.setOnClickListener(this);
        ListviewSearchResult.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {

                //String Slecteditem = SearchKey.get(position);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(LatSearch.get(position)), Double.parseDouble(LongSearch.get(position))),17));
               if (layout_head_parent_searchresult.isShown() && layout_parent_searchresult.isShown())
                {
                    layout_parent_searchresult.setVisibility(View.INVISIBLE);
                    layout_parent_searchresult.startAnimation(animDown_layout_parent_searchresult);
                    layout_head_parent_searchresult.setVisibility(View.INVISIBLE);
                    layout_head_parent_searchresult.startAnimation(animUp_layout_head_parent_searchresult);

                }
            }
        });
       // ListviewSearchResult.setOnItemClickListener(this);

    }
 private void CheckFirstTime()
 {
     circleProgres.setVisibility(View.VISIBLE);
     if (prefs.getBoolean("firstrun", true))
     {


         ConnectivityManager connMgr = (ConnectivityManager)
                 getSystemService(Context.CONNECTIVITY_SERVICE);
         NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
         if (networkInfo != null && networkInfo.isConnected())
         {
             // GetDataJsonFromServer();
             GetDataJsonFirstRun();

         }
         else
         {

             GetDataJsonFirstRun();

         }
     }
     else
     {
         GetDataJsonFromServer();

     }

 }
    private void SingleClickBottomShet ()
    {
       ll.setVisibility(View.VISIBLE);
       ll.startAnimation(animUp);
       mMap.getUiSettings().setAllGesturesEnabled(false);
       checkGestureMaps = false;
    }
    @Override
    protected void onResume()
    {
        Log.e("Android", "onResume() event");
        super.onResume();
    }

  /*private void timer()
  {
      Log.e("Android", "timer() event");

      timer = new Timer();
      timer.scheduleAtFixedRate(new TimerTask() {

          @Override
          public void run() {
              // TODO Auto-generated method stub
              runOnUiThread(new Runnable() {
                  public void run() {
                      textTimer.setText(String.valueOf(TimeCounter));
                      prosesMenampilkanKapal();
                      //  timer.cancel();


                      TimeCounter++;
                  }
              });

          }
      }, 1000, 1000);
  }*/


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {

        getMenuInflater().inflate(R.menu.maps_activity_menu, menu);
        return false;
    }




   private void polylinedraw()
   {
           PolylineOptions options = new PolylineOptions().width(2).color(Color.BLUE).geodesic(true);
           for (int i = 0; i < points.size(); i++)
           {
               LatLng point = points.get(i);
               options.add(point);
           }
           line = mMap.addPolyline(options);

   }

   private void setUpMapIfNeeded()
   {
        Log.e("Android", "setUpMapIfNeeded() event");
        if (mMap == null)
        {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
             if (mMap != null)
            {
                setUpMap();
            }
        }
    }

    private void setUpMap()
    {   Log.e("Android", "setUpMap() event");

        Double LatitudeIndonesia  = 6.1750;
        Double LongitudeIndonesia = 106.8283;
        LatLng KordinatIndonesia    = new LatLng(LatitudeIndonesia,LongitudeIndonesia);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(KordinatIndonesia));
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.next_button:

              if (!PosisiMarkerSekarang.equalsIgnoreCase(TitikAkhirMarkerIDnHashValue))
                {
                    if (BackButton.isEnabled() == false)
                    {

                        BackButton.setEnabled(true);
                    }
                    Marker showInfo = null;
                    if (showInfo != null)
                    {
                        showInfo.remove();
                    }
                    int  PosisiMarkerSekarangInt =  Integer.parseInt(PosisiMarkerSekarang.replace("m",""));
                    Log.e("PosisiMarkerSekarang",String.valueOf(PosisiMarkerSekarangInt));
                    int PosisiMarkerSekarangIntSelanjutnya = PosisiMarkerSekarangInt+1;
                    String PosisiMarkerSekarangIntSelanjutnyaFinal = "m"+String.valueOf(PosisiMarkerSekarangIntSelanjutnya);
                    PosisiMarkerSekarang = PosisiMarkerSekarangIntSelanjutnyaFinal;
                    List<String> value = HashmarkerMap.get(PosisiMarkerSekarangIntSelanjutnyaFinal);
                    TimeStampReceiverHashValue = value.get(0);
                    ToSternHashValue   = value.get(1);
                    ROTHashValue        = value.get(2);
                    ShipTypeHashValue   = value.get(3);
                    IMONumberHashValue  = value.get(4);
                    DangerHeadHashValue = value.get(5);
                    DraughtHashValue    = value.get(6);
                    ETAMinuteHashValue  = value.get(7);
                    ETADayHashValue     = value.get(8);
                    NavStatusHashValue  = value.get(9);
                    StationIDHashValue  = value.get(10);
                    CallSignHashValue   = value.get(11);
                    ZoneIDHashValue     = value.get(12);
                    COGHashValue        = value.get(13);
                    HDGHashValue        = value.get(14);
                    ETAHourHashValue    = value.get(15);
                    StationNameHashValue= value.get(16);
                    ZoneNameHashValue   = value.get(17);
                    GrtHashValue        = value.get(18);
                    ToPortHashValue     = value.get(19);
                    ETAMonthHashValue   = value.get(20);
                    SOGHashValue        = value.get(21);
                    DangerDistHashValue = value.get(22);
                    ToBowHashValue      = value.get(23);
                    GrtBySiukHashValue  = value.get(24);
                    ToStarBoardHashValue= value.get(25);
                    NavStatusDescriptionHashValue = value.get(26);
                    DestinationHashValue= value.get(27);
                    MMSIHashValue       = value.get(28);
                    ShipTypeDescription = value.get(29);
                    ShipNameHashValue   = value.get(30);
                    TypeHashValue       = value.get(31);
                    StringLatitudeJsonHashValue = value.get(32);
                    StringLongitudeJsonHashValue = value.get(33);
                    TitikAwalMarkerIDHashValue = value.get(34);
                    TitikAkhirMarkerIDnHashValue = value.get(35);

                    KordinatKapal = new LatLng(Double.parseDouble(StringLatitudeJsonHashValue),Double.parseDouble(StringLongitudeJsonHashValue));
                    int PosisiIndexMarker = Integer.parseInt(PosisiMarkerSekarang.replace("m", ""));
                    showInfo =  MarkerList.get(PosisiIndexMarker);
                    showInfo.showInfoWindow();
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(KordinatKapal,10));

                }
                else
                {
                     NextButton.setEnabled(false);
                }



                break;
            case R.id.back_button:

                if (!PosisiMarkerSekarang.equalsIgnoreCase(TitikAwalMarkerIDHashValue))
                {
                    if (NextButton.isEnabled() == false)
                    {
                        NextButton.setEnabled(true);
                    }
                    Marker showInfo = null;
                    if (showInfo != null)
                    {
                        showInfo.remove();
                    }
                    int  PosisiMarkerSekarangInt =  Integer.parseInt(PosisiMarkerSekarang.replace("m",""));
                    int PosisiMarkerSekarangIntSebelumnya = PosisiMarkerSekarangInt-1;
                    String PosisiMarkerSebelumnyaIntSelanjutnyaFinal = "m"+String.valueOf(PosisiMarkerSekarangIntSebelumnya);
                    PosisiMarkerSekarang = PosisiMarkerSebelumnyaIntSelanjutnyaFinal;
                    //Toast.makeText(getBaseContext(),"baru"+PosisiMarkerSebelumnyaIntSelanjutnyaFinal,Toast.LENGTH_SHORT).show();
                    List<String> value = HashmarkerMap.get(PosisiMarkerSebelumnyaIntSelanjutnyaFinal);
                    TimeStampReceiverHashValue = value.get(0);
                    ToSternHashValue   = value.get(1);
                    ROTHashValue        = value.get(2);
                    ShipTypeHashValue   = value.get(3);
                    IMONumberHashValue  = value.get(4);
                    DangerHeadHashValue = value.get(5);
                    DraughtHashValue    = value.get(6);
                    ETAMinuteHashValue  = value.get(7);
                    ETADayHashValue     = value.get(8);
                    NavStatusHashValue  = value.get(9);
                    StationIDHashValue  = value.get(10);
                    CallSignHashValue   = value.get(11);
                    ZoneIDHashValue     = value.get(12);
                    COGHashValue        = value.get(13);
                    HDGHashValue        = value.get(14);
                    ETAHourHashValue    = value.get(15);
                    StationNameHashValue= value.get(16);
                    ZoneNameHashValue   = value.get(17);
                    GrtHashValue        = value.get(18);
                    ToPortHashValue     = value.get(19);
                    ETAMonthHashValue   = value.get(20);
                    SOGHashValue        = value.get(21);
                    DangerDistHashValue = value.get(22);
                    ToBowHashValue      = value.get(23);
                    GrtBySiukHashValue  = value.get(24);
                    ToStarBoardHashValue= value.get(25);
                    NavStatusDescriptionHashValue = value.get(26);
                    DestinationHashValue= value.get(27);
                    MMSIHashValue       = value.get(28);
                    ShipTypeDescription = value.get(29);
                    ShipNameHashValue   = value.get(30);
                    TypeHashValue       = value.get(31);
                    StringLatitudeJsonHashValue = value.get(32);
                    StringLongitudeJsonHashValue = value.get(33);
                    TitikAwalMarkerIDHashValue = value.get(34);
                    TitikAkhirMarkerIDnHashValue = value.get(35);

                    KordinatKapal = new LatLng(Double.parseDouble(StringLatitudeJsonHashValue),Double.parseDouble(StringLongitudeJsonHashValue));
                    int PosisiIndexMarker = Integer.parseInt(PosisiMarkerSekarang.replace("m", ""));
                    showInfo =  MarkerList.get(PosisiIndexMarker);
                    showInfo.showInfoWindow();
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(KordinatKapal,10));


                }
                else
                {
                    BackButton.setEnabled(false);
                }


                break;
            case R.id.DetailInfo:
                if (AlertDialogInfoKapalDetail == false)
                {
                    alertKapalDetail.showDialog(MapsActivity.this, ShipNameHashValue, MMSIHashValue,
                            CallSignHashValue, ToBowHashValue,ToSternHashValue, ToPortHashValue,ToStarBoardHashValue,
                            GrtHashValue,DestinationHashValue,ShipTypeDescription,SOGHashValue, COGHashValue,
                            HDGHashValue, NavStatusDescriptionHashValue,DraughtHashValue,StringLatitudeJsonHashValue,
                            StringLongitudeJsonHashValue,ETAHourHashValue,ETAMinuteHashValue,
                            ETADayHashValue,ETAMonthHashValue,TimeStampReceiverHashValue);
                    AlertDialogInfoKapalDetail = true;

                }

                break;

            case R.id.Toolbar_Reload_Button:
              // Toolbar_Reload_Button.setVisibility(View.INVISIBLE);
                //Toolbar_Reload_Button.startAnimation(animDownToolbar);
                CheckFirstTime();
               // onResume();
                break;

            case R.id.searchView:
                //Toast.makeText(getBaseContext(), " onclick ", Toast.LENGTH_LONG).show();
                if (!layout_head_parent_searchresult.isShown() && !layout_parent_searchresult.isShown())
                {
                    layout_parent_searchresult.setVisibility(View.VISIBLE);
                    layout_parent_searchresult.startAnimation(animUp_layout_parent_searchresult);
                    layout_head_parent_searchresult.setVisibility(View.VISIBLE);
                    layout_head_parent_searchresult.startAnimation(animDown_layout_head_parent_searchresult);

                }
                StatusChangeBackButton = true;
                break;
            case R.id.button_listkapal:
               /* if(ll.isShown())
                {
                    ll.setVisibility(View.GONE);
                }
                alertNavigation.showDialog(MapsActivity.this);*/

                if(ll.isShown())
                {
                    ll.setVisibility(View.GONE);
                    mMap.getUiSettings().setAllGesturesEnabled(true);
                    checkGestureMaps = true;
                }

                if(layout_parent_searchresult.isShown() && layout_head_parent_searchresult.isShown())
                {
                    layout_parent_searchresult.setVisibility(View.GONE);
                    layout_head_parent_searchresult.setVisibility(View.GONE);
                }
                if (AlertDialogDaftarKapal == false)
                {
                    alertNavigation.showDialog(MapsActivity.this);
                    AlertDialogDaftarKapal = true;
                }
                break;
            case R.id.fab:

                if (AlertDialogInfoKapalDetail == false)
                {
                    alertKapalDetail.showDialog(MapsActivity.this, ShipNameHashValue, MMSIHashValue,
                            CallSignHashValue, ToBowHashValue,ToSternHashValue, ToPortHashValue,ToStarBoardHashValue,
                            GrtHashValue,DestinationHashValue,ShipTypeDescription,SOGHashValue, COGHashValue,
                            HDGHashValue, NavStatusDescriptionHashValue,DraughtHashValue,StringLatitudeJsonHashValue,
                            StringLongitudeJsonHashValue,ETAHourHashValue,ETAMinuteHashValue,
                            ETADayHashValue,ETAMonthHashValue,TimeStampReceiverHashValue);
                    AlertDialogInfoKapalDetail = true;
                }
                break;


        }
    }
    @Override
    public boolean onMarkerClick(Marker marker)
    {
        if (NextButton.isEnabled() == false)
        {
           NextButton.setEnabled(true);
        }
        if(BackButton.isEnabled() == false)
        {
            BackButton.setEnabled(true);

        }

        indexCameraClick    = index;
        PosisiMarkerSekarang= marker.getId();
        List<String> value  = HashmarkerMap.get(marker.getId());

        TimeStampReceiverHashValue = value.get(0);
        ToSternHashValue    = value.get(1);
        ROTHashValue        = value.get(2);
        ShipTypeHashValue   = value.get(3);
        IMONumberHashValue  = value.get(4);
        DangerHeadHashValue = value.get(5);
        DraughtHashValue    = value.get(6);
        ETAMinuteHashValue  = value.get(7);
        ETADayHashValue     = value.get(8);
        NavStatusHashValue  = value.get(9);
        StationIDHashValue  = value.get(10);
        CallSignHashValue   = value.get(11);
        ZoneIDHashValue     = value.get(12);
        COGHashValue        = value.get(13);
        HDGHashValue        = value.get(14);
        ETAHourHashValue    = value.get(15);
        StationNameHashValue= value.get(16);
        ZoneNameHashValue   = value.get(17);
        GrtHashValue        = value.get(18);
        ToPortHashValue     = value.get(19);
        ETAMonthHashValue   = value.get(20);
        SOGHashValue        = value.get(21);
        DangerDistHashValue = value.get(22);
        ToBowHashValue      = value.get(23);
        GrtBySiukHashValue  = value.get(24);
        ToStarBoardHashValue= value.get(25);
        NavStatusDescriptionHashValue = value.get(26);
        DestinationHashValue= value.get(27);
        MMSIHashValue       = value.get(28);
        ShipTypeDescription = value.get(29);
        ShipNameHashValue   = value.get(30);
        TypeHashValue       = value.get(31);
        StringLatitudeJsonHashValue = value.get(32);
        StringLongitudeJsonHashValue = value.get(33);
        TitikAwalMarkerIDHashValue = value.get(34);
        TitikAkhirMarkerIDnHashValue = value.get(35);


            if (TitikAwalMarkerIDHashValue.equalsIgnoreCase("Detail") || TitikAkhirMarkerIDnHashValue.equalsIgnoreCase("Detail"))
            {
                if( !floatingActionButton.isShown())
                {
                    floatingActionButton.setVisibility(View.VISIBLE);
                    floatingActionButton.startAnimation(flow_button_visible);
                }
            }
            else
            {
                SingleClickBottomShet();
                toolbar.setTitle(ShipNameHashValue);
            }


        Marker showInfo;
        int PosisiIndexMarker = Integer.parseInt(PosisiMarkerSekarang.replace("m", ""));
        showInfo =  MarkerList.get(PosisiIndexMarker);
        showInfo.showInfoWindow();
        Namakapal.setText(ShipNameHashValue);
        TanggalKapal.setText(TimeStampReceiverHashValue);
        return true;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {  switch (v.getId())
        {
            case R.id.my_toolbar:
                switch (event.getAction())
                {
                    case MotionEvent.ACTION_DOWN :
                    {
                        startY = event.getY();
                        break ;
                    }
                    case MotionEvent.ACTION_UP:
                    {
                        float endY = event.getY();

                        if (endY > startY)
                        {
                            ll.startAnimation(animDown);
                            ll.setVisibility(View.GONE);
                            //mMap.getUiSettings().setScrollGesturesEnabled(true);
                            mMap.getUiSettings().setAllGesturesEnabled(true);
                           // markerName.showInfoWindow();
                            checkGestureMaps = true;
                        }
                    }
                }
            break;
            case R.id.searchView:
                searchView.setFocusable(true);
                break;

        }

        return true;
    }
    @Override
    public void onBackPressed()
    {
        if (!searchView.isIconified())
        {
            searchView.setIconified(true);
            if (layout_head_parent_searchresult.isShown() && layout_parent_searchresult.isShown())
            {
                //Toast.makeText(getBaseContext(),"1", Toast.LENGTH_LONG).show();
                layout_parent_searchresult.setVisibility(View.INVISIBLE);
                layout_parent_searchresult.startAnimation(animDown_layout_parent_searchresult);
                layout_head_parent_searchresult.setVisibility(View.INVISIBLE);
                layout_head_parent_searchresult.startAnimation(animUp_layout_head_parent_searchresult);

            }
            else
            {
                if (durasiNotificationFirstTime == true)
                {
                    durasi.cancel();
                }
                System.exit(0);
                //onDestroy();
            }
        } else
        {

            super.onBackPressed();
        }

    }
    @Override
    public View getInfoWindow(Marker marker)
    {
        // Tampilan info window marker default
        return null;
    }

    @Override
    public View getInfoContents(Marker marker)
    {


        View v         = getLayoutInflater().inflate(R.layout.info_marker, null);
        Namakapal      = (TextView) v.findViewById(R.id.NamaKapal);
        TanggalKapal   = (TextView) v.findViewById(R.id.TanggalKapal);
        JamKapal       = (TextView) v.findViewById(R.id.JamKapal);
        KecepatanKapal = (TextView) v.findViewById(R.id.KecepatanKapal);

        Namakapal.setText(ShipNameHashValue);
        String a = TimeStampReceiverHashValue.replace(".", "");
        String[] parts = a.split("E");
        String input = parts[0];
        DateFormat inputDF = new SimpleDateFormat("yyyyMMddHHmmss");
        DateFormat outputDF = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat outputDF2 = new SimpleDateFormat("HH:mm:ss");

        Date date = null;
        try
        {
            date = inputDF.parse(""+input);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        if (date != null)
        {
            TanggalKapal.setText(String.valueOf(outputDF.format(date)));
            JamKapal.setText(String.valueOf(outputDF2.format(date)));
        }
        KecepatanKapal.setText(SOGHashValue+" K");

        return v;
    }

    public void getJson() throws IOException, JSONException
    {
        List<LatLng> valuesBatasAwalDanAkhir = new ArrayList<LatLng>();
        Boolean TitikAwalKapal = true;
        String TitikAwalMarkerID = null,TitikAkhirMarkerID = null;
        JSONArray array = new JSONArray(JsonData);

        JSONArray ArrayJsonSemuaKapal = array.getJSONArray(0);

        for (int i = 0; i < ArrayJsonSemuaKapal.length(); i++)
        {
            JSONObject JsonPerKapal =  ArrayJsonSemuaKapal.getJSONObject(i);
            JSONArray features = (JSONArray) JsonPerKapal.get("features");

         for (int j = 0; j < features.length(); j++)
            {
                JSONObject Feature          = (JSONObject) features.get(j);
                JSONObject geometry         = (JSONObject) Feature.get("geometry");
                JSONObject properties       = (JSONObject) Feature.get("properties");
                String TimeStampReceiver    = properties.optString("TimeStampReceiver");
                String ToStern              = properties.optString("ToStern");
                String ROT                  = properties.optString("ROT");
                String ShipType             = properties.optString("ShipType");
                String IMONumber            = properties.optString("IMONumber");
                String DangerHead           = properties.optString("DangerHead");
                String Draught              = properties.optString("Draught");
                String ETAMinute            = properties.optString("ETAMinute");
                String ETADay               = properties.optString("ETADay");
                String NavStatus            = properties.optString("NavStatus");
                String StationID            = properties.optString("StationID");
                String CallSign             = properties.optString("CallSign");
                String ZoneID               = properties.optString("ZoneID");
                String COG                  = properties.optString("COG");
                String HDG                  = properties.optString("HDG");
                String ETAHour              = properties.optString("ETAHour");
                String StationName          = properties.optString("StationName");
                String ZoneName             = properties.optString("ZoneName");
                String Grt                  = properties.optString("Grt");
                String ToPort               = properties.optString("ToPort");
                String ETAMonth             = properties.optString("ETAMonth");
                String SOG                  = properties.optString("SOG");
                String DangerDist           = properties.optString("DangerDist");
                String ToBow                = properties.optString("ToBow");
                String GrtBySiuk            = properties.optString("GrtBySiuk");
                String ToStarBoard          = properties.optString("ToStarBoard");
                String NavStatusDescription = properties.optString("NavStatusDescription");
                String Destination          = properties.optString("Destination");
                String MMSI                 = properties.optString("MMSI");
                String ShipTypeDescription  = properties.optString("ShipTypeDescription");
                String ShipName             = properties.optString("ShipName");
                String type                 = geometry.optString("type");
                JSONArray  coordinates      = (JSONArray) geometry.get("coordinates");
                String StringLatitudeJson  = String.valueOf(coordinates.get(1));
                String StringLongitudeJson = String.valueOf(coordinates.get(0));;

                Double LatitudeJson = Double.valueOf(StringLatitudeJson);
                Double LongitudeJson = Double.valueOf(StringLongitudeJson);
                LatLng Kordinat    = new LatLng(LatitudeJson,LongitudeJson);


                if(j == features.length()-1)
                {
                    //=======================simpan arraylist daftar kapal
                    ListNamaKapal.add(ShipName);
                    ListMMSI.add(MMSI);
                    LatKapal.add(StringLatitudeJson);
                    LongtKapal.add(StringLongitudeJson);

                    ListTimeStamp.add(TimeStampReceiver);
                    //======================================================================= simpan ke database (awal)
                    dbWrite = DbHelper.getWritableDatabase();
                    values.put(DbHelper.COLUMN_MMSI ,MMSI);
                    values.put(DbHelper.COLUMN_ShipName,ShipName);
                    values.put(DbHelper.COLUMN_StringLatitudeJson,StringLatitudeJson);
                    values.put(DbHelper.COLUMN_StringLongitudeJson,StringLongitudeJson);
                    dbWrite.insert(DbHelper.TABLE_NAME, null, values);
                    dbWrite.close();

                }
                //======================================================================= simpan ke database (akhir)


                points.add(Kordinat);
                if(TitikAwalKapal == true)
                {
                    markerName =  mMap.addMarker(new MarkerOptions().position(Kordinat).icon(BitmapDescriptorFactory.fromResource(R.drawable.awal_kapal)).title(ShipName));
                    TitikAwalMarkerID = markerName.getId();
                    TitikAwalKapal = false;
                    if ( CameraAwal == false)
                    {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Kordinat, 5));
                        CameraAwal = true;
                    }
                    valuesBatasAwalDanAkhir.add(Kordinat);

                }
                else if (TitikAwalKapal == false && j != features.length()-1)
                {

                    markerName =  mMap.addMarker(new MarkerOptions().position(Kordinat).icon(BitmapDescriptorFactory.fromResource(R.drawable.jalur_kapal)).title(ShipName));

                }
                else if(TitikAwalKapal == false && j == features.length()-1)
                {

                    valuesBatasAwalDanAkhir.add(Kordinat);
                    TextView text = new TextView(getBaseContext());
                    text.setText(ShipName);

                    markerName =  mMap.addMarker(new MarkerOptions().position(Kordinat).icon(BitmapDescriptorFactory.fromResource(R.drawable.kargo_titik)).title(ShipName));
                    TitikAkhirMarkerID = markerName.getId();
                    polylinedraw();
                }


                List<String> values = new ArrayList<String>();
                values.add(TimeStampReceiver);
                values.add(ToStern);
                values.add(ROT);
                values.add(ShipType);
                values.add(IMONumber);
                values.add(DangerHead);
                values.add(Draught);
                values.add(ETAMinute);
                values.add(ETADay);
                values.add(NavStatus);
                values.add(StationID);
                values.add(CallSign);
                values.add(ZoneID);
                values.add(COG);
                values.add(HDG);
                values.add(ETAHour);
                values.add(StationName);
                values.add(ZoneName);
                values.add(Grt);
                values.add(ToPort);
                values.add(ETAMonth);
                values.add(SOG);
                values.add(DangerDist);
                values.add(ToBow);
                values.add(GrtBySiuk);
                values.add(ToStarBoard);
                values.add(NavStatusDescription);
                values.add(Destination);
                values.add(MMSI);
                values.add(ShipTypeDescription);
                values.add(ShipName);
                values.add(type);
                values.add(StringLatitudeJson);
                values.add(StringLongitudeJson);
                values.add(TitikAwalMarkerID);
                values.add(TitikAkhirMarkerID);
                HashmarkerMap.put(markerName.getId(),values);
                MarkerList.add(markerName);

            }
            TitikAwalKapal = true;
            if(!points.isEmpty())
            {
                points.clear();

            }
            TitikAwalMarkerID = null;
            TitikAkhirMarkerID = null;

            if(i==ArrayJsonSemuaKapal.length()-1)
            {

                circleProgres.setVisibility(View.INVISIBLE);

            }
        }

        //================================================================ memunculkan searach view

        if (JsonData != null)
        {

            layout_parent_search.setVisibility(View.VISIBLE);
            layout_parent_search.startAnimation(Searchview_Effect);
        }

        //================================================================ memunculkan search view

    }
    private void GetDataJsonFromServer()
    {

        String REGISTER_URL = "http://27.111.46.52/modabile.dlu/api/vessel/getall/";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        JsonData = response;
                        if (JsonData !=null)
                        {
                            try
                            {
                                File file;
                                FileOutputStream outputStream;

                                try
                                {
                                    file = new File(getCacheDir(),"MyCacheLastData");
                                    outputStream = new FileOutputStream(file);
                                    outputStream.write(response.getBytes());
                                    outputStream.close();
                                } catch (IOException e)
                                {
                                    e.printStackTrace();
                                }

                                getJson();
                                circleProgres.setVisibility(View.INVISIBLE);
                            }
                            catch (IOException e)
                            {
                               circleProgres.setVisibility(View.INVISIBLE);
                                Toast.makeText(getBaseContext(),e.toString(),Toast.LENGTH_LONG).show();
                                readcache();
                            }
                            catch (JSONException e)
                            {
                               circleProgres.setVisibility(View.INVISIBLE);
                                Toast.makeText(getBaseContext(),e.toString(),Toast.LENGTH_LONG).show();
                                readcache();

                            }
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        circleProgres.setVisibility(View.INVISIBLE);
                        Toast.makeText(getBaseContext(),"Koneksi internet anda bermasalah", Toast.LENGTH_LONG).show();
                        readcache();
                    }
                }) {
        };
        stringRequest.setTag("Json");
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 10000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    private void GetDataJsonFirstRun()
    {

        String REGISTER_URL = "http://27.111.46.52/modabile.dlu/api/vessel/getall/";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        JsonData = response;
                        if (JsonData !=null)
                        {
                            prefs.edit().putBoolean("firstrun", false).commit();

                            if (Layout_Toolbar_Reload_Button.isShown())
                            {
                                Layout_Toolbar_Reload_Button.setVisibility(View.GONE);
                                Layout_Toolbar_Reload_Button.startAnimation(animDownToolbar);

                            }

                            try
                            {
                                File file;
                                FileOutputStream outputStream;

                                try
                                {

                                    file = new File(getCacheDir(),"MyCacheLastData");
                                    outputStream = new FileOutputStream(file);
                                    outputStream.write(response.getBytes());
                                    outputStream.close();
                                } catch (IOException e)
                                {
                                    circleProgres.setVisibility(View.INVISIBLE);
                                    e.printStackTrace();
                                }
                                getJson();
                            }
                            catch (IOException e)
                            {
                                Toast.makeText(getBaseContext(),e.toString(),Toast.LENGTH_LONG).show();
                                circleProgres.setVisibility(View.INVISIBLE);
                            }
                            catch (JSONException e)
                            {
                               Toast.makeText(getBaseContext(),e.toString(),Toast.LENGTH_LONG).show();
                                circleProgres.setVisibility(View.INVISIBLE);
                            }

                            circleProgres.setVisibility(View.INVISIBLE);
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {


                       if (timerdownload == false)
                        {
                            timerdownload();
                        }
                        else if(ReloadPertamaKali == false)
                        {
                            if(circleProgres.isShown())
                            {
                                circleProgres.setVisibility(View.INVISIBLE);
                            }
                            Toast.makeText(getBaseContext(),"Koneksi internet anda bermasalah",Toast.LENGTH_SHORT).show();


                        }

                    }
                }) {
        };
        stringRequest.setTag("Json");
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 100000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);


    }
    private void readcache ()
    {
        String line;
        BufferedReader input = null;
        File file = null;
        StringBuilder textfile = new StringBuilder();
        try
        {

            file = new File(getCacheDir(), "MyCacheLastData");
            input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

            //StringBuffer buffer = new StringBuffer();
            while ((line = input.readLine()) != null)
            {
                textfile.append(line);
            }

        } catch (IOException e)
        {
            e.printStackTrace();
        }


        if(textfile.toString() != null)
        {
            JsonData = textfile.toString();
            try
            {
                Toast.makeText(getBaseContext(), "Koneksi internet anda bermasalah", Toast.LENGTH_LONG).show();
                getJson();
            } catch (IOException e)
            {
                e.printStackTrace();
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getBaseContext(),"Tidak ada data sebelumnya yang bisa di tampilkan",Toast.LENGTH_SHORT).show();
        }
    }

    public void timerdownload()
    {



            timerdownload = true;
            durasi = new CountDownTimer(30000, 1000)
            {

                public void onTick(long millisUntilFinished)
                {

                    durasiNotificationFirstTime = true;
                    if((PercobaanAmbilData  % 2) == 0)
                    {   if (JsonData == null)
                    {
                        GetDataJsonFirstRun();
                    }

                    }

                    if (JsonData != null &&  durasiNotificationFirstTime == true)
                    {
                        durasi.cancel();
                    }
                    PercobaanAmbilData++;
                }

                public void onFinish()
                {

                    durasiNotificationFirstTime = false;
                    ReloadPertamaKali = false;
                    circleProgres.setVisibility(View.INVISIBLE);
                    if (JsonData == null)
                    {
                        //Toolbar_Reload_Button.setVisibility(View.VISIBLE);
                        //Toolbar_Reload_Button.startAnimation(animUpToolbar);
                        Layout_Toolbar_Reload_Button.setVisibility(View.VISIBLE);
                        Layout_Toolbar_Reload_Button.startAnimation(animUpToolbar);
                        Toast.makeText(getBaseContext(), "Koneksi internet anda bermasalah", Toast.LENGTH_SHORT).show();
                    }

                }
            }.start();
    }


    @Override
    public boolean onQueryTextSubmit(String query)
    {   Toast.makeText(getBaseContext(),"text : "+query, Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText)
    {
        if (newText.length() >1)
         {
             LatSearch.clear();
             LongSearch.clear();
             SearchKey.clear();
             SearchKeyName.clear();
             boolean CheckInput;
             CustomListAdapterListSearchResult adapter = new CustomListAdapterListSearchResult(this,SearchKey,SearchKeyName);

             if( floatingActionButton.isShown())
             {
                 floatingActionButton.setVisibility(View.INVISIBLE);
                 floatingActionButton.startAnimation(flow_button_invisible);

             }
            if (!layout_head_parent_searchresult.isShown() && !layout_parent_searchresult.isShown())
            {
                layout_parent_searchresult.setVisibility(View.VISIBLE);
                layout_parent_searchresult.startAnimation(animUp_layout_parent_searchresult);
                layout_head_parent_searchresult.setVisibility(View.VISIBLE);
                layout_head_parent_searchresult.startAnimation(animDown_layout_head_parent_searchresult);

            }

            if(ll.isShown())
            {
                ll.setVisibility(View.GONE);
            }

                String regex = "[0-9]+";
                CheckInput = newText.matches(regex);
                if (CheckInput == true)
                {

                    SQLiteDatabase dbRead = DbHelper.getReadableDatabase();
                    String selectQuery = "SELECT * FROM Json_Data_Kapal WHERE MMSI LIKE '%"+newText+"%'";
                    Cursor cursor = dbRead.rawQuery(selectQuery, null);

                    if (cursor.moveToFirst())
                    {
                        do
                        {
                            SearchKey.add(cursor.getString(0));
                            SearchKeyName.add(cursor.getString(1));
                            LatSearch.add(cursor.getString(2));
                            LongSearch.add(cursor.getString(3));

                        } while (cursor.moveToNext());


                    }

                    ListviewSearchResult.setAdapter(adapter);
                    ListviewSearchResult.setEmptyView(data_tidak_ada);



                }
                else
                {

                    SQLiteDatabase dbRead = DbHelper.getReadableDatabase();
                    String selectQuery = "SELECT * FROM Json_Data_Kapal WHERE ShipName LIKE '%"+newText+"%'";
                    Cursor cursor = dbRead.rawQuery(selectQuery, null);
                    if (cursor.moveToFirst())
                    {
                        do
                        {

                            SearchKey.add(cursor.getString(1));
                            SearchKeyName.add(cursor.getString(0));
                            LatSearch.add(cursor.getString(2));
                            LongSearch.add(cursor.getString(3));

                        } while (cursor.moveToNext());
                    }

                        ListviewSearchResult.setAdapter(adapter);
                        ListviewSearchResult.setEmptyView(data_tidak_ada);

                }
         }
        return false;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        switch(view.getId())
        {
            case R.id.listview_search_result:

                String Slecteditem= SearchKey.get(position);
                Toast.makeText(getBaseContext(),Slecteditem, Toast.LENGTH_SHORT).show();
                Toast.makeText(getBaseContext(), "tes", Toast.LENGTH_SHORT).show();
                if (layout_head_parent_searchresult.isShown() && layout_parent_searchresult.isShown())
                {
                    layout_parent_searchresult.setVisibility(View.INVISIBLE);
                    layout_parent_searchresult.startAnimation(animDown_layout_parent_searchresult);
                    layout_head_parent_searchresult.setVisibility(View.INVISIBLE);
                    layout_head_parent_searchresult.startAnimation(animUp_layout_head_parent_searchresult);

                }
                break;
        }
    }

    @Override
    public void onMapClick(LatLng latLng)
    {
        if(floatingActionButton.isShown())
        {
            floatingActionButton.setVisibility(View.INVISIBLE);
            floatingActionButton.startAnimation(flow_button_invisible);
        }
    }
}
