package mapsship.project.mapsship;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by plato on 3/31/2016.
 */
public class Filter_view_kapal_dynamic_listview_adaptor extends ArrayAdapter
{

    private final Activity context;
    ArrayList<String> MMSISubmit = new ArrayList<String>();
    ArrayList<String>  NamaKapalSubmit = new ArrayList<String>();
    public  Filter_view_kapal_dynamic_listview_adaptor(Activity context, ArrayList<String>  MMSISubmit ,ArrayList<String> NamaKapalSubmit)
    {
        super(context, R.layout.filter_view_kapal_listview_textview, MMSISubmit);
        this.context = context;
        this.MMSISubmit = MMSISubmit;
        this. NamaKapalSubmit =  NamaKapalSubmit;
    }

    public View getView(int position,View view,ViewGroup parent)
    {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.filter_view_kapal_listview_textview, null, true);

        TextView TextviewMMSISubmit = (TextView) rowView.findViewById(R.id.TextviewMMSISubmit);
        TextView TextviewNamaKapalSubmit = (TextView) rowView.findViewById(R.id.TextviewNamaKapalSubmit);


        TextviewMMSISubmit.setText(MMSISubmit .get(position));
        TextviewNamaKapalSubmit.setText( NamaKapalSubmit.get(position));

        return rowView;

    }
}
