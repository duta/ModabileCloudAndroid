package mapsship.project.mapsship;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by plato on 3/29/2016.
 */
public class Navigation_Page_Listview extends ArrayAdapter
{
    private final Activity context;
    ArrayList<Integer> GambarMenu = new ArrayList<Integer>();
    ArrayList<String> NamaMenu = new ArrayList<String>();
    public Navigation_Page_Listview(Activity context, ArrayList<Integer> GambarMenu ,ArrayList<String> NamaMenu)
    {
        super(context, R.layout.navigation_page_listview_menu, NamaMenu);
        this.context = context;
        this.GambarMenu =  GambarMenu;
        this.NamaMenu = NamaMenu;
    }
    public View getView(int position,View view,ViewGroup parent)
    {

        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.navigation_page_listview_menu, null, true);

        ImageView GambarMenuLList = (ImageView) rowView.findViewById(R.id.imageViewGambarMenu);
        TextView KeteranganMenu = (TextView) rowView.findViewById(R.id.KeteranganMenu);


        GambarMenuLList.setImageResource(GambarMenu.get(position));
        KeteranganMenu.setText(NamaMenu.get(position));


        return rowView;

    }

}
