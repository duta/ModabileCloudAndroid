package mapsship.project.mapsship;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.text.SimpleDateFormat;

/**
 * Created by ILHAM on 4/13/2016.
 */
public class filter_view_kapal_dynamic_thread extends Thread
{
    private final Handler mHandler; //handler dari UI
    private Context context;

    public  filter_view_kapal_dynamic_thread(Context context, Handler mHandler)
    {
        this.mHandler = mHandler;
        this.context = context;
    }

    public void run()
    {

        int y;
        SimpleDateFormat df;
        String URL;
        String dateFrom_params = null, dateTo_params = null;
        for (int i = 0; i<Filter_view_kapal_dynamic.MMSItersubmit.size(); i++)
        { Filter_view_kapal_dynamic.mmsi_params = Filter_view_kapal_dynamic.MMSItersubmit.get(i);
            for (int x = 0; x<Filter_view_kapal_dynamic.daftarWaktu.size(); x++)
          // for (int x = 0; x<Filter_view_kapal_dynamic.daftarWaktu.size()-1; x++)
            {
                if(x != Filter_view_kapal_dynamic.daftarWaktu.size()-1)
                {

                    df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    dateFrom_params = String.valueOf(df.format(Filter_view_kapal_dynamic.daftarWaktu.get(x)));
                    dateFrom_params = dateFrom_params.replaceAll("\\s", "%20");

                    y = x+1;
                    dateTo_params = String.valueOf(df.format(Filter_view_kapal_dynamic.daftarWaktu.get(y)));
                    dateTo_params = dateTo_params.replaceAll("\\s", "%20");

                   /* Calendar calTo = Calendar.getInstance();
                    calTo.setTime(Filter_view_kapal_dynamic.daftarWaktu.get(y));
                    calTo.add(Calendar.SECOND,59);
                    dateTo_params = df.format(calTo.getTime());
                    dateTo_params = dateTo_params.replaceAll("\\s", "%20");*/

                    URL = "http://27.111.46.52/modabile.dlu/api/vessel/getrecorded/?mmsi="+Filter_view_kapal_dynamic.mmsi_params+"&dateFrom="+dateFrom_params+"&dateTo="+dateTo_params+"";
                    final String finalURL = URL;
                    StringRequest req  = new StringRequest(Request.Method.GET,URL,
                            new Response.Listener<String>()
                            {
                                @Override
                                public void onResponse(String response)
                                {
                                    Log.e("Respon", finalURL);
                                    Log.e("Respon", response);

                                }
                            },
                            new Response.ErrorListener()
                            {
                                @Override
                                public void onErrorResponse(VolleyError error)
                                {


                                    Log.e("Error", error.toString());
                                }
                            })
                    {
                    };
                    VolleySingelton.getInstance(context).addToRequestQueue(req);

                }

            }

        }


    }


}
